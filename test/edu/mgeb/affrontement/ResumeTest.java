package edu.mgeb.affrontement;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import edu.mgeb.protagonistes.EquipeType;

class ResumeTest {
	
	private Resume resume;
	
	@BeforeEach
	void setUp() throws Exception {
		this.resume = new Resume();
	}

	@AfterEach
	void tearDown() throws Exception {
		this.resume = null;
	}

	@Test
	void testStock() {
		for (int i = 0; i<3;i++) {
			this.resume.addAction(EquipeType.MARIO);
			this.resume.addAction(EquipeType.LAPINS);
		}
		//Output must be 101010
		List<EquipeType> list = this.resume.getResume();
		assertEquals(list.size(),6);
		assertEquals(list.get(0),EquipeType.MARIO);
		assertEquals(list.get(1),EquipeType.LAPINS);
		assertEquals(list.get(2),EquipeType.MARIO);
		assertEquals(list.get(3),EquipeType.LAPINS);
		assertEquals(list.get(4),EquipeType.MARIO);
		assertEquals(list.get(5),EquipeType.LAPINS);
	}
	
	@Test
	void testReverse() {
		for (int i = 0; i<3;i++) {
			this.resume.addAction(EquipeType.MARIO);
			this.resume.addAction(EquipeType.LAPINS);
		}
		this.resume.reverse();
		//Output must be 010101
		List<EquipeType> list = this.resume.getResume();
		assertEquals(list.size(),6);
		assertEquals(list.get(5),EquipeType.MARIO);
		assertEquals(list.get(4),EquipeType.LAPINS);
		assertEquals(list.get(3),EquipeType.MARIO);
		assertEquals(list.get(2),EquipeType.LAPINS);
		assertEquals(list.get(1),EquipeType.MARIO);
		assertEquals(list.get(0),EquipeType.LAPINS);
	}
	
	@Test
	void testAddResume() {
		for (int i = 0; i<3;i++) {
			this.resume.addAction(EquipeType.LAPINS);
		}
		Resume newResume = new Resume();
		for (int i = 0; i<3;i++) {
			newResume.addAction(EquipeType.MARIO);
		}
		this.resume.addResume(newResume);
		//Output must be 000111
		List<EquipeType> list = this.resume.getResume();
		assertEquals(list.size(),6);
		assertEquals(list.get(0),EquipeType.LAPINS);
		assertEquals(list.get(1),EquipeType.LAPINS);
		assertEquals(list.get(2),EquipeType.LAPINS);
		assertEquals(list.get(3),EquipeType.MARIO);
		assertEquals(list.get(4),EquipeType.MARIO);
		assertEquals(list.get(5),EquipeType.MARIO);
	}
	
	@Test
	void testAddResumeAndTresor() {
		for (int i = 0; i<3;i++) {
			this.resume.addAction(EquipeType.LAPINS);
		}
		Resume newResume = new Resume();
		for (int i = 0; i<3;i++) {
			newResume.addAction(EquipeType.MARIO);
		}
		Tresor tresor = new Tresor();
		newResume.setLootable(tresor.getLoot());
		this.resume.addResume(newResume);
		//Output must be 000111
		List<EquipeType> list = this.resume.getResume();
		assertEquals(list.size(),6);
		assertEquals(list.get(0),EquipeType.LAPINS);
		assertEquals(list.get(1),EquipeType.LAPINS);
		assertEquals(list.get(2),EquipeType.LAPINS);
		assertEquals(list.get(3),EquipeType.MARIO);
		assertEquals(list.get(4),EquipeType.MARIO);
		assertEquals(list.get(5),EquipeType.MARIO);
		assertSame(tresor,this.resume.getLoot());
	}
	
	@Test
	void testAddResumeOnlyTresor() {
		for (int i = 0; i<3;i++) {
			this.resume.addAction(EquipeType.LAPINS);
		}
		Resume newResume = new Resume();
		Tresor tresor = new Tresor();
		newResume.setLootable(tresor.getLoot());
		this.resume.addResume(newResume);
		//Output must be 101010
		List<EquipeType> list = this.resume.getResume();
		assertEquals(list.size(),3);
		assertEquals(list.get(0),EquipeType.LAPINS);
		assertEquals(list.get(1),EquipeType.LAPINS);
		assertEquals(list.get(2),EquipeType.LAPINS);
		assertSame(tresor,this.resume.getLoot());
	}
	
	@Test
	void testAddResumeAndReverse() {
		//insert 111000
		for (int i = 0; i<3;i++) {
			this.resume.addAction(EquipeType.MARIO);
		}
		Resume newResume = new Resume();
		for (int i = 0; i<3;i++) {
			newResume.addAction(EquipeType.LAPINS);
		}
		this.resume.addResume(newResume);
		this.resume.reverse();
		//Output must be 000111
		List<EquipeType> list = this.resume.getResume();
		assertEquals(list.size(),6);
		assertEquals(list.get(0),EquipeType.LAPINS);
		assertEquals(list.get(1),EquipeType.LAPINS);
		assertEquals(list.get(2),EquipeType.LAPINS);
		assertEquals(list.get(3),EquipeType.MARIO);
		assertEquals(list.get(4),EquipeType.MARIO);
		assertEquals(list.get(5),EquipeType.MARIO);
	}

}
