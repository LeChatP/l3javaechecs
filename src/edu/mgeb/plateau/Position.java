package edu.mgeb.plateau;

import java.io.Serializable;

/**
 * Classe définissant une position dans le plateau avec ses limites
 * 
 * @author lechatp, Matthieu Gallea
 */
public class Position implements Serializable {

	private static final long serialVersionUID = -2364189345695146069L;
	private char lettre;
	private int position;

	/**
	 * Constructeur vérifiant que la lettre et la position existe,
	 * 
	 * @param lettre   définissant les Y
	 * @param position chiffre des x
	 * @throws IllegalArgumentException si la position est impossible sur le
	 *                                  plateau, exception très probable, à gérer
	 *                                  dans tous les algorithmes créant des
	 *                                  positions
	 */
	public Position(char lettre, int position) throws IllegalArgumentException {
		if ((lettre < 'A' || lettre > 'H') || (position < 0 || position > 7))
			throw new IllegalArgumentException("La position doit être dans le plateau : " + lettre + " " + position);
		this.lettre = lettre;
		this.position = position;
	}

	/**
	 * @return la lettre de la position
	 */
	public char getLettre() {
		return lettre;
	}

	/**
	 * @return le chiffre de la position
	 */
	public int getPosition() {
		return position;
	}

	public String getFullPosition() {
		return new StringBuilder().append(this.lettre).append(this.position).toString();
	}

	/**
	 * Renvoie l'index par rapport à un tableau unidimensionnel
	 * 
	 * @return index dans un tableau
	 */
	public int toIndex() {
		return (lettre - 'A') * 8 + position;
	}

	/**
	 * Revoie un index quand le tableau est inversé sur l'axe des X et Y
	 * 
	 * @return l'inverse de toIndex()
	 */
	public int invertIndex() {
		return (lettre - 'A') + position * 8;
	}

	/**
	 * Fonction de hachage pour les Set
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + lettre;
		result = prime * result + position;
		return result;
	}

	/**
	 * Fonction d'égalité pour simplifier les comparaison par les coordonées
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Position other = (Position) obj;
		if (lettre != other.lettre)
			return false;
		if (position != other.position)
			return false;
		return true;
	}

}
