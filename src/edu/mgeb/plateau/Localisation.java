package edu.mgeb.plateau;

import edu.mgeb.protagonistes.EtreVivant;

/**
 * Classe Conservant un être vivant et sa position dans les cas où seul ces
 * informations sont nécessaire
 * 
 * @author lechatp, Matthieu Gallea
 */
public class Localisation {
	private Position pos;
	private EtreVivant etre;

	public Localisation(Position pos, EtreVivant etre) {
		this.pos = pos;
		this.etre = etre;
	}

	public Position getPos() {
		return pos;
	}

	public EtreVivant getEtre() {
		return etre;
	}
}
