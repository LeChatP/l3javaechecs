package edu.mgeb.plateau;

import java.io.Serializable;

import edu.mgeb.protagonistes.Equipe;
import edu.mgeb.protagonistes.EquipeType;
import edu.mgeb.protagonistes.EtreVivant;
import edu.mgeb.protagonistes.Goomba;
import edu.mgeb.protagonistes.Lapin;
import edu.mgeb.protagonistes.TypeEnrolable;
import edu.mgeb.vue.ILivre;

/**
 * Classe plateau contenant les cases du jeu
 * 
 * @author lechatp, Matthieu Gallea
 */
public class Plateau implements Serializable {

	private static final long serialVersionUID = -4535683590880439933L;
	public static final int SIZE_MAP = 8;
	/**
	 * Cases du plateau
	 */
	private Case[] cases;
	/**
	 * Les equipes présentes dans le plateau
	 */
	private Equipe j1, j2;

	/**
	 * Constructeur du plateau
	 * 
	 * @param livre l'interface graphique
	 */
	public Plateau(ILivre livre) {
		this.cases = new Case[SIZE_MAP * SIZE_MAP];
		for (int i = 0; i < SIZE_MAP * SIZE_MAP; i++) {
			this.cases[i] = new Case(new Position((char) ('A' + i / 8), i % 8), livre);
		}
	}

	/**
	 * Fonction initialisant les équipes et plaçant les pions sur le plateau
	 * 
	 * @return
	 */
	public Case[] initialiserPlateau() {

		this.j2 = new Equipe(EquipeType.LAPINS);
		this.j1 = new Equipe(EquipeType.MARIO);
		int i = -1;
		for (EtreVivant e : this.j2.getList()) {
			this.cases[++i].setPion(e);
			if (this.cases[i].getPion().getTypeEnrolable() == TypeEnrolable.GOOMBA) {
				((Lapin) this.cases[i].getPion()).setInitial(new Position((char) ('A' + i / 8), i % 8));
			}
		}
		for (i++; i < 47; i++) {
			this.cases[i].viderCase();
		}
		for (EtreVivant e : this.j1.getList()) {
			this.cases[++i].setPion(e);
			if (e instanceof Goomba)
				((Goomba) e).setInitial(new Position((char) ('A' + i / 8), i % 8));
		}
		return this.cases;
	}

	public void finDuJeu(ILivre livre, EquipeType gagnant) {
		livre.message(gagnant.name() + " a gagné!"); // on annonce le gagnant
	}

	/**
	 * Vérifie que le jeu est terminé pour l'un des deux joueurs
	 */
	public EquipeType checkGameEnds(ILivre livre) {
		return checkWinner(livre, EquipeType.MARIO) ? EquipeType.MARIO
				: checkWinner(livre, EquipeType.LAPINS) ? EquipeType.LAPINS : null;
	}

	/**
	 * on vérifie si le joueur a perdu et annonce le gagne et termine la partie
	 * 
	 * @param joueur
	 * @return
	 */
	private boolean checkWinner(ILivre livre, EquipeType joueur) {
		if (checkLoose(joueur)) { // si le joueur a perdu
			this.finDuJeu(livre, joueur.adversaire());
			return true;
		}
		return false;
	}

	/**
	 * On vérifie que le joueur a perdu (il n'a plus de Dame
	 * 
	 * @param joueur
	 * @return vrai si il plus de dame, faux si il en a encore
	 */
	private boolean checkLoose(EquipeType joueur) {
		return !this.getEquipe(joueur).exists(TypeEnrolable.PEACH); // on vérifie que Peach n'existe plus dans l'équipe
	}

	/**
	 * Met à jour l'interface graphique du plateau avec les informations du plateau
	 * 
	 * @param livre interface graphique
	 */
	public void updatePlateau(ILivre livre) {
		for (Case cas : this.cases) {
			cas.updateCase(livre);
		}
	}

	/**
	 * Récuère tous les pions d'un joueur
	 * 
	 * @param retourne l'équipe correspondant à son type
	 * @return l'Equipe du joueur
	 */
	public Equipe getEquipe(EquipeType equipeType) {
		return equipeType.estMario() ? this.j1 : this.j2;
	}

	/**
	 * Vérifie que la position contient un ennemi de j1
	 * 
	 * @param p  Position indiquée
	 * @param j1 joueur actuel
	 * @return vrai si le pion appartient à un autre joueur.
	 */
	public boolean isEnemy(Position p, EquipeType j1) {
		return this.getEquipe(j1.adversaire()).getList().contains(this.getPion(p));
	}

	/**
	 * @return le tableau de toutes les cases
	 */
	public Case[] getCases() {
		return this.cases;
	}

	/**
	 * Obtient la case en fonction de sa position
	 * 
	 * @param p la position indiquée
	 * @return la case de la position
	 */
	public Case getCase(Position p) {
		return this.cases[p.toIndex()];
	}

	/**
	 * Obtient directement le pion de la case à la position p
	 * 
	 * @param p
	 * @return null si aucun pion n'est dans la case
	 */
	public EtreVivant getPion(Position p) {
		return this.getCase(p).getPion();
	}

	/**
	 * Vide le pion de la case à la position p
	 * 
	 * @param p
	 */
	public void vider(Position p) {
		this.getCase(p).viderCase();
	}

	/**
	 * Place le pion e à la position p
	 * 
	 * @param e
	 * @param p
	 */
	public void setPion(EtreVivant e, Position p) {
		this.getCase(p).setPion(e);
	}

	/**
	 * Déplace le pion d'une position from à la position to en gérant les affichages
	 * de l'interface graphique
	 * 
	 * @param from
	 * @param to
	 */
	public void deplacer(ILivre livre, Position from, Position to) {
		EtreVivant etre = this.getPion(from);
		this.setPion(etre, to);
		this.vider(from);
		livre.ecrireDescription(
				etre.getNom() + " en " + from.getFullPosition() + " se déplace en " + to.getFullPosition());
	}

	/**
	 * Vérifie que la case à la position p possède un pion
	 * 
	 * @param p
	 * @return Vrai si un Etrevivant est dans la case
	 */
	public boolean estOccupe(Position p) {
		return this.getCase(p).estOccupe();
	}
}
