package edu.mgeb.plateau;

import java.io.Serializable;

import edu.mgeb.protagonistes.EquipeType;
import edu.mgeb.vue.ILivre;

/**
 * 
 * @author lechatp
 *
 */
public class Tour implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2357420719277608569L;

	/**
	 * Role du joueur en ligne
	 */
	private EquipeType joueur;

	/**
	 * Si le jeu est en local
	 */
	private boolean local;

	/**
	 * A qui est le tour?
	 */
	private EquipeType tourDe;

	/**
	 * Livre pour afficher le tour
	 */
	private transient ILivre livre;

	/**
	 * Le jeu est en pause ? null si non, une equipe si une des deux équipe a mis en
	 * pause
	 */
	private EquipeType pause;

	/**
	 * Le gagnant de la partie
	 */
	private EquipeType gagnant;

	public Tour(ILivre livre) {
		this.livre = livre;
		this.joueur = null;
		this.local = true;
		this.pause = null;
		this.tourDe = null;
		this.setGagnant(null);
	}

	/**
	 * Gestion privé du setter Local
	 * 
	 * @param local
	 */
	private void setLocal(boolean local) {
		this.local = local;
	}

	/**
	 * gestion privée du changement de tour
	 * 
	 * @param tourDe
	 */
	private void setTourDe(EquipeType tourDe) {
		if (this.enPause())
			return;
		this.tourDe = tourDe;
		this.afficherTourDe();
	}

	/**
	 * Gestion privée da l'affichage du tour du joueur
	 */
	public void afficherTourDe() {
		if (this.enPause())
			this.livre.afficherMessage("La partie a été mis en pause par " + this.pause.toString() + " ("
					+ (this.joueur == this.pause ? "Vous" : "L'adversaire") + ")");
		else if (!this.sonTour()) // si ce n'est pas son tour c'est qu'il joue en ligne
			this.livre.afficherMessage("Au tour de l'adversaire (" + this.tourDe.toString() + ")");
		else if (!local) // Si ce n'est pas en ligne c'est que c'est sont tour
			this.livre.afficherMessage("A votre tour (" + this.tourDe.toString() + ")");
		else // autrement on est en local.
			this.livre.afficherMessage("Au tour de l'équipe " + this.tourDe.toString());
	}

	/**
	 * Met le jeu en local
	 */
	public void setLocal() {
		this.setLocal(true);
		this.setPause(null);
		this.setTourDe(EquipeType.randomEquipe());
	}

	public void setPause(EquipeType equipepause) {
		this.pause = equipepause;
	}

	public void resumePause() {
		this.pause = null;
	}

	public boolean enPause() {
		return this.pause != null;
	}

	public EquipeType equipeEnPause() {
		return this.pause;
	}

	/**
	 * Met le jeu en ligne
	 * 
	 * @param joueur demande le rôle du joueur
	 * @param tourDe demande le tour du joueur
	 */
	public void setOnline(EquipeType joueur, EquipeType tourDe) {
		this.setLocal(false);
		this.joueur = joueur;
		this.setPause(null);
		this.setTourDe(tourDe);
	}

	/**
	 * Vérifie si c'est bien le tour du joueur
	 * 
	 * @return Vrai si en local. Variable en fonction de la partie en ligne
	 */
	public boolean sonTour() {
		return !this.enPause() && (this.enLocal() || this.joueur == this.tourDe);
	}

	/**
	 * Retourne l'équipe qui joue
	 * 
	 * @return
	 */
	public EquipeType getTourDe() {
		return this.tourDe;
	}

	/**
	 * 
	 * @return
	 */
	public boolean enLocal() {
		return this.local;
	}

	/**
	 * Change le tour de la partie
	 */
	public void passerLeTour() {
		this.setTourDe(this.tourDe.adversaire());
	}

	public EquipeType monEquipe() {
		return this.joueur;
	}

	public void setLivre(ILivre livre) {
		this.livre = livre;
	}

	public boolean hasWinner() {
		return gagnant != null;
	}

	public EquipeType getGagnant() {
		return gagnant;
	}

	public void setGagnant(EquipeType gagnant) {
		this.gagnant = gagnant;
	}

}
