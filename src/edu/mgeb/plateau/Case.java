package edu.mgeb.plateau;

import java.io.Serializable;

import edu.mgeb.protagonistes.Equipe;
import edu.mgeb.protagonistes.EtreVivant;
import edu.mgeb.vue.ILivre;

/**
 * Classe définissant une case sur le plateau
 * 
 * @author lechatp, Matthieu Gallea
 */
public class Case implements Serializable {

	private static final long serialVersionUID = 2331962754887530688L;
	/**
	 * L'etre vivant sur la case (si placé dessus)
	 */
	private EtreVivant pion;
	/**
	 * le livre pour les animations et l'affichage GUI
	 */
	private transient ILivre livre;
	/**
	 * La position de la case
	 */
	private Position pos;

	/**
	 * Constructeur de la case définissant sa position et les possibilités de
	 * l'interface graphique
	 * 
	 * @param pos
	 * @param livre
	 */
	public Case(Position pos, ILivre livre) {
		this.pion = null;
		this.pos = pos;
		this.livre = livre;
	}

	/**
	 * Vérifie la présence s'un pion dans la case
	 * 
	 * @return vrai si un etrevivant est dans la case
	 */
	public boolean estOccupe() {
		return this.pion != null;
	}

	/**
	 *
	 * @return La position de la case par rapport au plateau
	 */
	public Position getPosition() {
		return this.pos;
	}

	/**
	 * Place le pion dans la case et l'affiche
	 * 
	 * @param e
	 */
	public void setPion(EtreVivant e) {
		this.pion = e;
		livre.afficherPion(pos.getLettre(), pos.getPosition(), Equipe.findEquipe(e), e.getNom(), e.getVie(),
				e.getAttaque(), e.getDefense(), e.getMagie(), e.getImage());
	}

	/**
	 * Renvoie le pion dans la casse
	 * 
	 * @return null si aucun pion, l'êtrevivant dans le cas contraire
	 */
	public EtreVivant getPion() {
		return this.pion;
	}

	/**
	 * Retire le pion de la case Vide la case dans l'interface Graphique
	 */
	public void viderCase() {
		this.pion = null;
		livre.retirerPion(pos.getLettre(), pos.getPosition());
	}

	/**
	 * Met a jout les informations de la case Met à jour l'interface avec le nouveau
	 * livre donné en paramètre
	 * 
	 * @used Deserialisation
	 * @param livre2
	 */
	public void updateCase(ILivre livre2) {
		this.livre = livre2;
		if (this.pion == null) {
			livre.retirerPion(pos.getLettre(), pos.getPosition());
		} else {
			livre.afficherPion(pos.getLettre(), pos.getPosition(), Equipe.findEquipe(this.pion), this.pion.getNom(),
					this.pion.getVie(), this.pion.getAttaque(), this.pion.getDefense(), this.pion.getMagie(),
					this.pion.getImage());
		}
	}

}
