package edu.mgeb.plateau;

import java.io.Serializable;

import edu.mgeb.affrontement.Bataille;
import edu.mgeb.affrontement.Resume;
import edu.mgeb.affrontement.ResumeReader;
import edu.mgeb.protagonistes.Equipe;
import edu.mgeb.protagonistes.EtreVivant;
import edu.mgeb.vue.ILivre;

/**
 * Classe de mouvement et interaction entre deux pions
 * 
 * @author lechatp, Matthieu Gallea
 */
public class Move implements Serializable {
	private static final long serialVersionUID = -5241307094756770149L;
	/**
	 * Les positions à déplacer. on conserve la source pour effectuer le deplacement
	 */
	protected Position from, to;

	/**
	 * Constructeur définissant la position source et destination
	 * 
	 * @param from source
	 * @param to   destination
	 */
	public Move(Position from, Position to) {
		this.from = from;
		this.to = to;
	}

	/**
	 * 
	 * @return Source
	 */
	public Position from() {
		return from;
	}

	/**
	 * 
	 * @return Destination
	 */
	public Position to() {
		return to;
	}

	/**
	 * Effectue le mouvement en déclarant une bataille si nécessaire. Exécute la
	 * fonction a la suite de manière asynchrone après toutes les animations
	 * 
	 * @param p     le plateau du jeu
	 * @param livre l'interface graphique pour les animations en cascade
	 * @param next  la prochaine fonction à éxecuter après toutes les animations
	 * @see Jeu, Bataille
	 * @return null si aucun combat, un résumé complet du combat si le combat à eu
	 *         lieu
	 */
	public Resume move(Plateau p, ILivre livre, Runnable next) {
		EtreVivant v = p.getPion(this.from);
		if (p.estOccupe(to)) {
			Bataille b = new Bataille(new Localisation(this.from, v), new Localisation(this.to, p.getPion(this.to)));

			return b.combattre(livre, () -> this.gagnant(p, livre, next), () -> this.perdant(p, livre, next));
		} else {
			p.deplacer(livre, from, to);
			if (next != null)
				next.run();
			return null;
		}
	}

	public void move(Plateau p, ILivre livre, Runnable next, Resume resume) {
		if (resume == null) { // si le résumé est vide alors pas de combat
			p.deplacer(livre, from, to);
			if (next != null)
				next.run();
		} else {
			Bataille b = new Bataille(new Localisation(this.from, p.getPion(this.from)),
					new Localisation(this.to, p.getPion(this.to)));
			b.combattre(livre, () -> this.gagnant(p, livre, next), () -> this.perdant(p, livre, next),
					new ResumeReader(resume));
		}
	}

	private void gagnant(Plateau p, ILivre livre, Runnable next) {
		EtreVivant perdant = p.getPion(this.to); // on récupère le pion
		p.getEquipe(Equipe.findEquipe(perdant)).remove(perdant); // on supprime le pion
		p.deplacer(livre, from, this.to);
		if (next != null)
			next.run();
	}

	private void perdant(Plateau p, ILivre livre, Runnable next) {
		pionPerdu(p, livre);
		if (next != null)
			next.run();
	}

	/**
	 * Execute l'action quand le pion source a pardu la bataille Fonction exécutée
	 * de manière asynchrone
	 * 
	 * @see move
	 * @param p     Plateau
	 * @param livre Interface graphique
	 */
	public void pionPerdu(Plateau p, ILivre livre) {
		EtreVivant etre = p.getPion(from);
		p.getEquipe(Equipe.findEquipe(etre)).remove(etre);
		p.vider(from);

		EtreVivant pion = p.getPion(to);
		livre.afficherPion(to.getLettre(), to.getPosition(), Equipe.findEquipe(pion), pion.getNom(), pion.getVie(),
				pion.getAttaque(), pion.getDefense(), pion.getMagie(), pion.getImage());
	}

	/**
	 * Fonction de hachage basée sur la destination car les movements sont unique à
	 * une destination Vu qu'on ne fais qu'un mouvement par tour
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((to == null) ? 0 : to.hashCode());
		return result;
	}

	/**
	 * Fonction equals en conséquence
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Move other = (Move) obj;
		if (to == null) {
			if (other.to != null)
				return false;
		} else if (!to.equals(other.to))
			return false;
		return true;
	}

}
