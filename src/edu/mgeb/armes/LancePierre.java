package edu.mgeb.armes;

import edu.mgeb.affrontement.LootableType;
import edu.mgeb.affrontement.Rarete;

/**
 * Classe de l'arme Lance Pierre
 * 
 * @author lechatp, Matthieu Gallea
 *
 */
public class LancePierre extends Arme {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4038416706580361744L;

	public LancePierre(Rarete rarete) {
		super("Lance Pierre", 10, rarete);
	}

	@Override
	public LootableType getLootableType() {
		return LootableType.LANCEPIERRE;
	}

	@Override
	public String verbeAttaque() {
		return "tire";
	}
}
