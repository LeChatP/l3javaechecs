package edu.mgeb.armes;

import edu.mgeb.affrontement.Lootable;
import edu.mgeb.affrontement.Rarete;
import edu.mgeb.vue.ILivre;

/**
 * Classe abstraite de l'arme
 * 
 * @author lechatp, Matthieu Gallea
 *
 */
public abstract class Arme implements Lootable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4577047523496068662L;
	protected String nature;
	protected int degats;

	public Arme(String nature, int degats, Rarete rarete) {
		this.nature = nature + " " + rarete.name();
		this.degats = (int) Math.round(degats * rarete.getMultiplicateur());
	}

	public String getNature() {
		return nature;
	}

	public int getDegats() {
		return degats;
	}

	@Override
	public String toString() {
		return this.getNature();
	}

	public int attaque(ILivre livre) {
		return this.degats;
	}

	public String verbeAttaque() {
		return "attaque";
	}

	public String getDescription() {
		return this.nature + " avec " + this.degats + " de dégâts";
	}
}
