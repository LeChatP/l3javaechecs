package edu.mgeb.armes;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Enumération générant un nom aléatoirement pour les épées
 * 
 * @author lechatp, Matthieu Gallea
 */
public enum EpeeNameGenerator {
	A1("Betrayer"), A2("Armageddon"), A3("Stormcaller"), A4("Volcanic Shortsword"), A5("Dire Sculptor"),
	A6("Guardian's Adamantite Etcher"), A7("Roaring Glass Guardian"), A8("Interrogator"), A9("Alpha"), A10("Scar");

	private static final List<EpeeNameGenerator> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
	private static final int SIZE = VALUES.size();
	private static final Random RANDOM = new Random();

	private String nom;

	EpeeNameGenerator(String nom) {
		this.nom = nom;
	}

	public static String randomName() {
		return VALUES.get(RANDOM.nextInt(SIZE)).getNom();
	}

	public String getNom() {
		return nom;
	}

}
