package edu.mgeb.armes;

import edu.mgeb.affrontement.LootableType;
import edu.mgeb.affrontement.Rarete;
import edu.mgeb.vue.ILivre;

/**
 * Classe de l'épée
 * 
 * @author lechatp, Matthieu Gallea
 *
 */
public class Epee extends Arme {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7171454670725255688L;
	/**
	 * nom de l'épée
	 */
	protected String nom;

	/**
	 * sa rareté définit sa puissance
	 * 
	 * @param nom
	 * @param rarete
	 */
	public Epee(String nom, Rarete rarete) {
		super("Epée", 80, rarete);
		this.nom = nom;
	}

	public String getNom() {
		return nom;
	}

	@Override
	public String toString() {
		return this.getNom();
	}

	@Override
	public int attaque(ILivre livre) {
		return this.getDegats();
	}

	@Override
	public LootableType getLootableType() {
		return LootableType.EPEE;
	}

	@Override
	public String verbeAttaque() {
		return "tranche";
	}

	@Override
	public String getDescription() {
		return this.nature + " " + this.nom + " avec " + this.degats + " de dégâts";
	}

}
