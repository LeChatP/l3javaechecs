package edu.mgeb.armes;

import edu.mgeb.affrontement.LootableType;
import edu.mgeb.affrontement.Rarete;
import edu.mgeb.vue.ILivre;

/**
 * L'arc avec un nombre de flèches limité
 * 
 * @author lechatp, Matthieu Gallea
 *
 */
public class Arc extends Arme {

	/**
	 * identifiant de sérialisation
	 */
	private static final long serialVersionUID = -4239156261642878069L;
	/**
	 * Nombre de flèches
	 */
	private int fleches;
	/**
	 * Nom de l'arme
	 */
	private static final String NAME = "Arc";

	/**
	 * Constructeur de l'arme
	 * 
	 * @param fleches son nombre de flèches
	 * @param rarete  sa rareté multipliant son nombre de lfèches
	 */
	public Arc(int fleches, Rarete rarete) {
		super(NAME, 30, rarete);
		this.fleches = fleches;
		this.fleches *= rarete.getMultiplicateur();
	}

	/**
	 * effectue une attaque en retirant une flèche du carquois
	 */
	@Override
	public int attaque(ILivre livre) {
		if (this.getNombreFleches() > 0) {
			this.fleches--;
			return this.degats;
		} else {
			livre.ecrireDescription("qui n'a pas de flèches.");
			return 0;
		}

	}

	@Override
	public String verbeAttaque() {
		return "tire";
	}

	/**
	 * récupère les dégâts en fonction du nombre de flèches
	 * 
	 * @return 0 si plus de flèches
	 */
	@Override
	public int getDegats() {
		return fleches > 0 ? degats : 0;
	}

	/**
	 * @return nombre de flèches
	 */
	public int getNombreFleches() {
		return this.fleches;
	}

	/**
	 * Le type de l'arme
	 */
	@Override
	public LootableType getLootableType() {
		// TODO Auto-generated method stub
		return LootableType.ARC;
	}

	@Override
	public String getDescription() {
		return this.nature + " possédant " + this.getNombreFleches() + " flèches effectuant " + this.degats
				+ " de dégâts";
	}

}
