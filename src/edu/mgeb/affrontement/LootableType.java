package edu.mgeb.affrontement;

import edu.mgeb.armes.Arc;
import edu.mgeb.armes.Epee;
import edu.mgeb.armes.LancePierre;
import edu.mgeb.armure.Bottes;
import edu.mgeb.armure.Casque;
import edu.mgeb.armure.Jambieres;
import edu.mgeb.armure.Plastron;
import edu.mgeb.magie.SortAir;
import edu.mgeb.magie.SortEau;
import edu.mgeb.magie.SortFeu;
import edu.mgeb.magie.SortTerre;

/**
 * Liste les objets qui peuvent être obtenus en trésor
 * 
 * @author lechatp, Matthieu Gallea
 *
 */
public enum LootableType {
	ARC, EPEE, LANCEPIERRE, BOTTES, CASQUE, JAMBIERES, PLASTRON, AIR, EAU, FEU, TERRE;

	@Override
	public String toString() {
		return this.name().charAt(0) + this.name().toLowerCase().substring(1);
	}

	public static LootableType fromLootable(Lootable lootable) {
		if (lootable != null) {
			if (lootable instanceof Arc)
				return ARC;
			else if (lootable instanceof Epee)
				return EPEE;
			else if (lootable instanceof LancePierre)
				return LANCEPIERRE;
			else if (lootable instanceof Bottes)
				return BOTTES;
			else if (lootable instanceof Casque)
				return CASQUE;
			else if (lootable instanceof Jambieres)
				return JAMBIERES;
			else if (lootable instanceof Plastron)
				return PLASTRON;
			else if (lootable instanceof SortAir)
				return AIR;
			else if (lootable instanceof SortEau)
				return EAU;
			else if (lootable instanceof SortFeu)
				return FEU;
			else if (lootable instanceof SortTerre)
				return TERRE;
		}
		throw new IllegalArgumentException("No elements occurs.");
	}
}
