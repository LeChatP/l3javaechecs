package edu.mgeb.affrontement;

import java.io.Serializable;
import java.util.Random;

import edu.mgeb.armes.Arc;
import edu.mgeb.armes.Epee;
import edu.mgeb.armes.EpeeNameGenerator;
import edu.mgeb.armes.LancePierre;
import edu.mgeb.armure.Bottes;
import edu.mgeb.armure.Casque;
import edu.mgeb.armure.Jambieres;
import edu.mgeb.armure.Plastron;
import edu.mgeb.magie.SortAir;
import edu.mgeb.magie.SortEau;
import edu.mgeb.magie.SortFeu;
import edu.mgeb.magie.SortTerre;

/**
 * Classe générant un trésor de manière aléatoire, notament sa rareté.
 * 
 * @author lechatp, Matthieu Gallea
 *
 */
public class Tresor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6260026441567511387L;
	/**
	 * Nombres aléatoires définissant l'objet gagné
	 */
	private byte rareteRandom;
	private byte lootableRandom;

	/**
	 * Constructeur générant les nombres définisant la récompense
	 */
	public Tresor() {
		Random r = new Random(System.currentTimeMillis());
		this.rareteRandom = (byte) r.nextInt(100);
		this.lootableRandom = (byte) r.nextInt(100);
		r = null;
	}

	/**
	 * Obtient la rareté de la récompense
	 * 
	 * @return Normal,Légendaire,Epique ou Usé
	 */
	private Rarete getRarete() {
		Rarete rarete = Rarete.Normal;
		if (this.rareteRandom > 95) {
			rarete = Rarete.Legendaire;
		} else if (this.rareteRandom > 80) {
			rarete = Rarete.Epique;
		} else if (this.rareteRandom > 50) {
			rarete = Rarete.Usee;
		}
		return rarete;
	}

	/**
	 * Récupère le type d'objet à partir du nombre aléatoire
	 * 
	 * @return Le type d'objet
	 */
	public LootableType getLootable() {
		LootableType type = null;
		if (this.lootableRandom % 3 == 0) {
			if (this.lootableRandom > 60) {
				type = LootableType.LANCEPIERRE;
			} else if (this.lootableRandom > 30) {
				type = LootableType.EPEE;
			} else {
				type = LootableType.ARC;
			}
		} else if (this.lootableRandom % 3 == 1) {
			if (this.lootableRandom > 75) {
				type = LootableType.CASQUE;
			} else if (this.lootableRandom > 50) {
				type = LootableType.PLASTRON;
			} else if (this.lootableRandom > 25) {
				type = LootableType.JAMBIERES;
			} else {
				type = LootableType.BOTTES;
			}
		} else {
			if (this.lootableRandom > 75) {
				type = LootableType.AIR;
			} else if (this.lootableRandom > 50) {
				type = LootableType.EAU;
			} else if (this.lootableRandom > 25) {
				type = LootableType.FEU;
			} else {
				type = LootableType.TERRE;
			}
		}
		return type;
	}

	/**
	 * Instancie la récompense avec sa rareté
	 * 
	 * @return
	 */
	public Lootable getLoot() {
		Rarete rarete = this.getRarete();
		LootableType type = this.getLootable();
		switch (type) {
		case LANCEPIERRE:
			return new LancePierre(rarete);
		case EPEE:
			return new Epee(EpeeNameGenerator.randomName(), rarete);
		case ARC:
			return new Arc(10, rarete);
		case CASQUE:
			return new Casque(rarete);
		case PLASTRON:
			return new Plastron(rarete);
		case JAMBIERES:
			return new Jambieres(rarete);
		case BOTTES:
			return new Bottes(rarete);
		case AIR:
			return new SortAir(rarete);
		case EAU:
			return new SortEau(rarete);
		case FEU:
			return new SortFeu(rarete);
		case TERRE:
			return new SortTerre(rarete);
		}
		throw new IllegalStateException("Ce Lootable n'est pas référencé");
	}

}
