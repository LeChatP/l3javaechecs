package edu.mgeb.affrontement;

import java.io.Serializable;
import java.nio.BufferOverflowException;
import java.util.ArrayList;
import java.util.List;

import edu.mgeb.protagonistes.EquipeType;

/**
 * Classe résumé d'un combat
 * 
 * @author lechatp
 *
 */
public class Resume implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7604494172899296052L;
	/**
	 * Le résumé est une liste de booléens stockés dans un Long Permet de stocker 64
	 * booléens en 8 octets au lieu de 1 octet par booléen Un combat faisant de 8 à
	 * 16 attaques voire plus, l'objet ne fais plus que 11 octet de données fixe au
	 * lieu d'être variable
	 */
	private long resume;
	private byte length;
	private Lootable tresor;

	public Resume() {
		this.length = 0;
		this.setResume(0L);
		this.setLootable(null);
	}

	public Lootable getLoot() {
		return tresor;
	}

	public void setLootable(Lootable tresor) {
		this.tresor = tresor;
	}

	/**
	 * Tranforme un tous les bit d'un Long en une ArrayList de booléens
	 * 
	 * @return
	 */
	public List<EquipeType> getResume() {
		List<EquipeType> resume = new ArrayList<>();
		for (char i = 0; i < length; i++)
			resume.add(this.testBit(i));
		return resume;
	}

	public void setResume(long l) {
		this.resume = l;
	}

	public void addAction(EquipeType joueur) {
		if (this.length == Long.BYTES * 8)
			throw new BufferOverflowException(); // un Long ne contient pas plus de 64Bits
		if (joueur.estMario())
			this.setBit(this.length, (byte) 1);
		this.length += 1;
	}

	public void reverse() {
		for (char i = 0; i < this.length / 2; i++) {
			if (!(this.testBit(i) == this.testBit(this.length - 1 - i))) {
				this.flipBit(i);
				this.flipBit(this.length - 1 - i);
			}
		}
	}

	public void addResume(Resume combattre) {
		if (combattre.getLoot() != null) {
			this.tresor = combattre.getLoot();
		}
		for (char i = 0; i < combattre.length; i++) {
			this.addAction(combattre.testBit(i));
		}
	}

	private EquipeType testBit(int k) {
		return ((this.resume >> k) & 1) == 1 ? EquipeType.MARIO : EquipeType.LAPINS;
	}

	private void setBit(int k, byte value) {
		this.resume = this.resume | (value << k);
	}

	private void flipBit(int k) {
		this.resume = this.resume ^ (1 << k);
	}
}
