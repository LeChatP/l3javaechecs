package edu.mgeb.affrontement;

/**
 * Liste des Type de trésor
 * 
 * @author lechatp, Matthieu Gallea
 */
public enum TresorType {
	ARMURE, ARME, SORTILEGE;

	@Override
	public String toString() {
		return this.name().charAt(0) + this.name().toLowerCase().substring(1);
	}
}
