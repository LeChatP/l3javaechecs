/**
 * Package concernant les affrontements du jeu, et les gains a la fin des
 * combats
 */
package edu.mgeb.affrontement;
