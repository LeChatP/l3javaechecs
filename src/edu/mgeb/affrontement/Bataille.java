package edu.mgeb.affrontement;

import edu.mgeb.armes.Arme;
import edu.mgeb.armure.Armure;
import edu.mgeb.magie.Sortilege;
import edu.mgeb.plateau.Localisation;
import edu.mgeb.protagonistes.Equipe;
import edu.mgeb.protagonistes.EquipeType;
import edu.mgeb.protagonistes.EtreVivant;
import edu.mgeb.vue.ILivre;

/**
 * Classe Bataille customisée du jeu Fantaisies
 * 
 * @author lechatp, Matthieu Gallea, Aubin BERINGUET
 *
 */
public class Bataille {
	/**
	 * Une bataille possède deux protagonistes avec leur position Les positions ne
	 * servent qu'à l'interface graphique
	 */
	private Localisation attacker;
	private Localisation defender;

	/**
	 * Crée une bataille avec les deux protagoniqtes qui se combattent
	 * 
	 * @param attacker attaquant
	 * @param defender défenseur
	 */
	public Bataille(Localisation attacker, Localisation defender) {
		this.defender = defender;
		this.attacker = attacker;
	}

	/**
	 * Effectue le combat. La fonction est exécutée récursivement de manière
	 * asynchrone en synchronisation avec les évènements des animations
	 * 
	 * @param livre le livre pour effectuer les animations
	 * @param win   Action a effectuer si l'attaquant gagne
	 * @param loose Action a effectuer si le défenseur gagne
	 * @return Le résumé du combat
	 */
	public Resume combattre(ILivre livre, Runnable win, Runnable loose) {
		Resume resume = new Resume(); // en résumé on liste tous les dégâts effectués par chaqu'uns, puis on met un
										// Trésor
		if (attacker.getEtre().getVie() > 0 && defender.getEtre().getVie() > 0) { // tant qu'il y a un etre en vie
			EquipeType joueurQuiAttaque = EquipeType.randomEquipe();
			resume.addAction(joueurQuiAttaque); // ajout des actions en pré-récursif, protégeant de tout problème
												// synchrone ou asynchrone
			if (joueurQuiAttaque == Equipe.findEquipe(attacker.getEtre()))
				this.attaquer(livre, attacker, defender, win, loose, resume);
			else
				this.attaquer(livre, defender, attacker, win, loose, resume);
		} else
			resume.setLootable(loot(livre, win, loose)); // On crée simplement l'objet Tresor qui sera ajouté au résumé
		return resume;
	}

	/**
	 * Effectue le combat à partir des données du Résumé du serveur
	 * 
	 * @param livre  le livre pour effectuer les animations
	 * @param win    Action a effectuer si l'attaquant gagne
	 * @param loose  Action a effectuer si le défenseur gagne
	 * @param reader Le résumé des attaques effectués sur le serveur
	 */
	public void combattre(ILivre livre, Runnable win, Runnable loose, ResumeReader reader) {
		if (reader.hasNext()) { // tant qu'il y a un etre en vie
			EquipeType next = reader.next();
			if (next == Equipe.findEquipe(attacker.getEtre())) {
				this.attaquer(livre, attacker, defender, win, loose, reader);
			} else {
				this.attaquer(livre, defender, attacker, win, loose, reader);
			}
		} else {
			loot(livre, win, loose, reader.getLootable()); // on effectue le travail de loot a partir du trésor envoyé
		}
	}

	private void attaquer(ILivre livre, Localisation attacker, Localisation defender, Runnable win, Runnable loose,
			ResumeReader reader) {
		this.attaquer(livre, attacker, defender, win, loose, () -> combattre(livre, win, loose, reader));// on fais la
																											// récursion
																											// une fois
																											// que
																											// l'animation
																											// a terminé
	}

	private void attaquer(ILivre livre, Localisation attacker, Localisation defender, Runnable win, Runnable loose,
			Resume resume) {
		this.attaquer(livre, attacker, defender, win, loose, () -> resume.addResume(combattre(livre, win, loose)));
	}

	private void attaquer(ILivre livre, Localisation attacker, Localisation defender, Runnable win, Runnable loose,
			Runnable next) {
		/*
		 * Le Combat est effectué en pré-récursivité, donc l'ordre des coups est dans le
		 * bon ordre
		 */
		int i = attacker.getEtre().combattre(livre, defender.getEtre());
		livre.afficherSubirDegats(defender.getPos().getLettre(), defender.getPos().getPosition(), i,
				defender.getEtre().getDefense(), defender.getEtre().getMagie(), next); // on fais la récursion une fois
																						// que l'animation a terminé
	}

	/**
	 * Fais gagner un Trésor a la fin du combat
	 * 
	 * @param livre pour effectuer les animations
	 * @param win   action a effectuer quand l'attaquant gagne
	 * @param loose action à effectuer quand le défenseur gagne
	 * @return lo sort/armure/arme qui looté dans le trésor
	 */
	private Lootable loot(ILivre livre, Runnable win, Runnable loose) {
		Tresor tresor = new Tresor();
		return loot(livre, win, loose, tresor.getLoot());
	}

	/**
	 * Fais gagner un Trésor a la fin du combat
	 * 
	 * @param livre  pour effectuer les animations
	 * @param win    action a effectuer quand l'attaquant gagne
	 * @param loose  action à effectuer quand le défenseur gagne
	 * @param tresor le tresor gagné
	 * @return affiche et reinitialise le plateau
	 */
	private Lootable loot(ILivre livre, Runnable win, Runnable loose, Lootable tresor) {
		if (defender.getEtre().getVie() > 0) {
			this.ajouterLoot(livre, defender.getEtre(), tresor);
			// Waterfall of actions
			// On affiche les animations dans l'ordre des animations
			livre.afficherSoins(defender.getPos().getLettre(), defender.getPos().getPosition(),
					defender.getEtre().getDefaultVie() - defender.getEtre().getVie(), () -> {
						defender.getEtre().resetVie();
						livre.afficherGainTresor(defender.getPos().getLettre(), defender.getPos().getPosition(),
								LootableType.fromLootable(tresor), () -> {
									livre.afficherPion(defender.getPos().getLettre(), defender.getPos().getPosition(),
											Equipe.findEquipe(defender.getEtre()), defender.getEtre().getNom(),
											defender.getEtre().getVie(), defender.getEtre().getAttaque(),
											defender.getEtre().getDefense(), defender.getEtre().getMagie(),
											defender.getEtre().getImage());
									livre.mourirPion(attacker.getPos().getLettre(), attacker.getPos().getPosition(),
											loose);
								});
					});

		} else {
			this.ajouterLoot(livre, attacker.getEtre(), tresor);
			// Waterfall of actions
			// On affiche les animations dans l'ordre
			livre.afficherSoins(attacker.getPos().getLettre(), attacker.getPos().getPosition(),
					attacker.getEtre().getDefaultVie() - attacker.getEtre().getVie(), () -> {
						attacker.getEtre().resetVie();
						livre.afficherGainTresor(attacker.getPos().getLettre(), attacker.getPos().getPosition(),
								LootableType.fromLootable(tresor), () -> {
									livre.afficherPion(attacker.getPos().getLettre(), attacker.getPos().getPosition(),
											Equipe.findEquipe(attacker.getEtre()), attacker.getEtre().getNom(),
											attacker.getEtre().getVie(), defender.getEtre().getAttaque(),
											attacker.getEtre().getDefense(), attacker.getEtre().getMagie(),
											attacker.getEtre().getImage());
									livre.mourirPion(defender.getPos().getLettre(), defender.getPos().getPosition(),
											win);
								});
					});
		}
		return tresor;
	}

	/**
	 * Ajoute le Trésor à l'etre vivant
	 * 
	 * @param etre qui gagne l'objet
	 * @param loot l'objet gagné
	 */
	public void ajouterLoot(ILivre livre, EtreVivant etre, Lootable loot) {
		if (loot instanceof Arme) {
			etre.ajouterArme(livre, (Arme) loot);
		} else if (loot instanceof Armure) {
			etre.ajouterArmure(livre, (Armure) loot);
		} else {
			etre.ajouterSortilege(livre, (Sortilege) loot);
		}
	}

}
