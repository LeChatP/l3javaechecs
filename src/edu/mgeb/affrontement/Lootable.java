package edu.mgeb.affrontement;

import java.io.Serializable;

/**
 * Généralise les classes qui peuvent être une récompense
 * 
 * @author lechatp, Matthieu Gallea
 *
 */
public interface Lootable extends Serializable {
	/**
	 * Fonction pour obtenir le type de récompense
	 * 
	 * @return
	 */
	public LootableType getLootableType();
}
