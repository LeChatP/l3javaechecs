package edu.mgeb.affrontement;

/**
 * Applique une politique de rareté d'un objet gagné dans un trésor
 * 
 * @author lechatp, Matthieu Gallea
 *
 */
public enum Rarete {
	Legendaire(2.0), Epique(1.5), Normal(1), Usee(0.5);

	private double multiplicateur;

	Rarete(double d) {
		this.multiplicateur = d;
	}

	public double getMultiplicateur() {
		return multiplicateur;
	}
}
