package edu.mgeb.affrontement;

import java.util.Iterator;

import edu.mgeb.protagonistes.EquipeType;

/**
 * 
 * @author lechatp
 *
 */
public class ResumeReader implements Iterator<EquipeType> {
	private Lootable lootable;
	private Iterator<EquipeType> it;

	public ResumeReader(Resume resume) {
		this.lootable = resume.getLoot();
		this.it = resume.getResume().iterator();
	}

	public Lootable getLootable() {
		return this.lootable;
	}

	@Override
	public boolean hasNext() {
		return this.it.hasNext();
	}

	@Override
	public EquipeType next() {
		return this.it.next();
	}
}
