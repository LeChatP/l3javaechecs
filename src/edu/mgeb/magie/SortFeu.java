package edu.mgeb.magie;

import edu.mgeb.affrontement.LootableType;
import edu.mgeb.affrontement.Rarete;

/**
 * Sortilège de feu, faible contre l'eau, fort contre la terre
 * 
 * @author lechatp, Matthieu Gallea
 */
public class SortFeu extends Sortilege {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7100206830490403319L;

	public SortFeu(Rarete rarete) {
		super(SortType.FEU, 20, rarete);
		// TODO Auto-generated constructor stub
	}

	@Override
	public LootableType getLootableType() {
		// TODO Auto-generated method stub
		return null;
	}

}
