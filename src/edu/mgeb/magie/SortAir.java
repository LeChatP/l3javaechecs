package edu.mgeb.magie;

import edu.mgeb.affrontement.LootableType;
import edu.mgeb.affrontement.Rarete;

/**
 * Sortilège d'air, faible contre la terre, fort contre l'eau
 * 
 * @author lechatp, Matthieu Gallea
 */
public class SortAir extends Sortilege {

	/**
	 * 
	 */
	private static final long serialVersionUID = -778233178480090963L;

	public SortAir(Rarete rarete) {
		super(SortType.AIR, 20, rarete);
	}

	@Override
	public LootableType getLootableType() {
		return null;
	}

}
