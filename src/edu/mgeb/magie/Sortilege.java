package edu.mgeb.magie;

import edu.mgeb.affrontement.Lootable;
import edu.mgeb.affrontement.Rarete;
import edu.mgeb.vue.ILivre;

/**
 * Classe des sortilèges
 * 
 * @author lechatp, Matthieu Gallea
 */
public abstract class Sortilege implements Lootable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3223656720934191123L;
	protected int degats;
	protected SortType type;

	/**
	 * Contrusteur d'un sort
	 * 
	 * @param type   type du sort
	 * @param degats les dégâts générés par sort
	 * @param rarete sa rareté multipliant sa puissance
	 */
	public Sortilege(SortType type, int degats, Rarete rarete) {
		this.degats = (int) Math.round(degats * rarete.getMultiplicateur());
		this.type = type;
	}

	/**
	 * 
	 * @return les dégâts du sort
	 */
	public int getDegats() {
		return degats;
	}

	/**
	 * 
	 * @return le type de sort
	 */
	public SortType getType() {
		return type;
	}

	/**
	 * Calcule la réduction de dégâts du sort reçu
	 * 
	 * @param livre
	 * @param type      Type de sort attaquant
	 * @param puissance puissance du sort attaquant
	 * @return les dégâts réduits, 0 si attaquant faible, toute la puissance si
	 *         attaquant fort
	 */
	public int encaisser(ILivre livre, SortType type, int puissance) {
		switch (this.type) {
		case TERRE: // si je porte un sort TERRE
			return this.encaisser(livre, SortType.FEU, SortType.TERRE, SortType.EAU, puissance);
		case FEU:
			return this.encaisser(livre, SortType.EAU, SortType.AIR, SortType.FEU, puissance);
		case AIR:
			return this.encaisser(livre, SortType.TERRE, SortType.AIR, SortType.FEU, puissance);
		case EAU:
			return this.encaisser(livre, SortType.AIR, SortType.TERRE, SortType.EAU, puissance);
		}
		throw new IllegalArgumentException("Cet Element n'existe pas");
	}

	private int encaisser(ILivre livre, SortType faible, SortType egal, SortType egal1, int puissance) {
		if (type == faible) {
			livre.ecrireDescription(
					"Son sort de " + this.type.toString() + " est inefficace face au sort de " + type.toString());
			return puissance;
		} else if (type == egal || type == egal1) {
			livre.ecrireDescription("Son sort de " + this.type.toString()
					+ " encaisse la différence de puissance face au sort de " + type.toString());
			return puissance > this.degats ? puissance - this.degats : 0;
		} else {
			livre.ecrireDescription(
					"Son sort de " + this.type.toString() + " encaisse complètement les sorts de " + type.toString());
			return 0; // j'encaisse l'AIR
		}
	}

	public String getDescription() {
		return "sort de " + this.type.toString() + " avec une puissance de " + this.degats;
	}

}
