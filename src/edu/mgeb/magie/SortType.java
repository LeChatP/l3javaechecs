package edu.mgeb.magie;

/**
 * Liste des sorts
 * 
 * @author lechatp, Matthieu Gallea
 */
public enum SortType {
	EAU, FEU, AIR, TERRE;

	@Override
	public String toString() {
		return this.name().charAt(0) + this.name().toLowerCase().substring(1);
	}
}
