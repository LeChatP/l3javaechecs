package edu.mgeb.magie;

import edu.mgeb.affrontement.LootableType;
import edu.mgeb.affrontement.Rarete;

/**
 * Sortilège de terre, faible contre le feu, fort contre l'air
 * 
 * @author lechatp, Matthieu Gallea
 */
public class SortTerre extends Sortilege {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8295851254913079269L;

	public SortTerre(Rarete rarete) {
		super(SortType.TERRE, 20, rarete);
		// TODO Auto-generated constructor stub
	}

	@Override
	public LootableType getLootableType() {
		// TODO Auto-generated method stub
		return null;
	}

}
