package edu.mgeb.magie;

import edu.mgeb.affrontement.LootableType;
import edu.mgeb.affrontement.Rarete;

/**
 * Sortilège l'eau, faible contre l'air, fort contre le feu
 * 
 * @author lechatp, Matthieu Gallea
 */
public class SortEau extends Sortilege {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8170495022252941779L;

	public SortEau(Rarete rarete) {
		super(SortType.EAU, 20, rarete);
		// TODO Auto-generated constructor stub
	}

	@Override
	public LootableType getLootableType() {
		// TODO Auto-generated method stub
		return null;
	}

}
