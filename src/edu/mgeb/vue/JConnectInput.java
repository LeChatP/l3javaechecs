package edu.mgeb.vue;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import edu.mgeb.commun.CreationPartie;
import edu.mgeb.commun.JoinAddress;

/**
 * 
 * @author lechatp
 *
 */
public class JConnectInput extends JPanel {

	private static final long serialVersionUID = 1083385918872541098L;
	private JTextField address;
	private JTextField code;

	public JConnectInput() {
		this.address = new JTextField(25);
		this.address.setText("127.0.0.1");
		this.code = new JTextField(CreationPartie.NB_CHARS_CODE);
		this.add(new JLabel("Adresse Serveur :"));
		this.add(address);
		this.add(Box.createHorizontalStrut(30));
		this.add(new JLabel("Code du jeu :"));
		this.add(code);
	}

	public JoinAddress showFullDialog() {
		while (true)
			if (JOptionPane.showConfirmDialog(null, this, "Entrez une adresse et un code de jeu",
					JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION) {

				try {
					return new JoinAddress(this.getAddress(), this.getCode());
				} catch (UnknownHostException e) {
					Object[] options = { "OK" };
					JOptionPane.showOptionDialog(null, "L'adresse n'existe pas ou est incorrecte", "Erreur",
							JOptionPane.PLAIN_MESSAGE, JOptionPane.ERROR_MESSAGE, null, options, options[0]);
				}
			} else
				return null;
	}

	public InetAddress getAddress() throws UnknownHostException {
		return InetAddress.getByName(this.address.getText());
	}

	public String getCode() {
		return this.code.getText().toUpperCase();
	}
}