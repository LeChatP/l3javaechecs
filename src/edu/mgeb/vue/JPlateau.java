package edu.mgeb.vue;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import edu.mgeb.protagonistes.EquipeType;

/**
 * Grille du plateau
 * 
 * @author lechatp, Matthieu Gallea
 */
public class JPlateau extends JPanel {

	/**
	 * Les cases de la grille
	 */
	private JCase[] cases;
	private static final long serialVersionUID = -4264980600055539880L;

	public JPlateau() {
		GridLayout grid = new GridLayout(8, 8);
		this.setOpaque(true);
		this.cases = new JCase[8 * 8];
		this.setLayout(grid);
		this.setSize(500, 500);
		for (int i = 0; i < this.cases.length; i++) {
			this.cases[i] = new JCase(i, ((i % 2) == (i / 8) % 2));
			this.add(this.cases[i]);
			this.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
		}

		this.setPreferredSize(new Dimension(700, 700));
		this.setMaximumSize(new Dimension(700, 700));
		this.setVisible(true);
	}

	public void setCase(char lettre, int position, EquipeType joueur1, String nom, int vie, int atk, int def, int magie,
			String image) {
		this.cases[(lettre - 'A') * 8 + position].setPion(nom, atk, vie, def, magie, joueur1, image);
	}

	public JCase getCase(char lettre, int position) {
		return this.cases[(lettre - 'A') * 8 + position];
	}

	public JCase[] getCases() {
		return this.cases;
	}

	public void deplacer(char lettre, int position, char lettreto, int positionto) {
		JCase save = this.cases[lettreto - 'A' * 8 + positionto];
		this.add(this.getComponent(lettre - 'A' * 8 + position), lettreto - 'A' * 8 + positionto);
		this.add(save, (lettre - 'A') * 8 + position);
	}

	public void emptyCase(char lettre, int position) {
		this.cases[(lettre - 'A') * 8 + position].empty();
	}

	@Override
	public Dimension getPreferredSize() {
		// TODO Auto-generated method stub
		return new Dimension(super.getPreferredSize().height, super.getPreferredSize().height);
	}

}
