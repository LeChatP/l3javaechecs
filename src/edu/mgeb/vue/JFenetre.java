package edu.mgeb.vue;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.Collection;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

import edu.mgeb.affrontement.LootableType;
import edu.mgeb.plateau.Move;
import edu.mgeb.protagonistes.EquipeType;

/**
 * Fenêtre GUI
 * 
 * @author lechatp, Matthieu Gallea, Aubin BERINGUET
 */
public class JFenetre extends JFrame implements ILivre {

	private static final long serialVersionUID = 4042713508717400450L;

	private JLabel tour;
	private JPlateau plateau;
	private JMenuBar menuBar;
	private JPrompt prompt;
	private JMessage message;

	private JPanel jeu;

	/**
	 * Met en place l'interface et applique les Listeners donnés en paramètre
	 * 
	 * @param mouse
	 * @param buttons
	 */
	public JFenetre(MouseListener mouse, ActionListener buttons) {
		super("Les Echecs Crétins");
		this.prompt = new JPrompt();
		this.setupMenus(buttons);
		this.loadIcon();
		this.jeu = new JPanel();
		jeu.setLayout(new BorderLayout());
		BorderLayout border = new BorderLayout();
		JLabel title = new JLabel("Echecs de Mario et les lapins crétins", JLabel.CENTER);
		Font f = title.getFont();
		this.plateau = new JPlateau();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setOpacity(1.0f);
		title.setFont(f.deriveFont(f.getStyle() | Font.BOLD));
		this.setLayout(border);
		JPanel north = new JPanel(new GridLayout(2, 1));
		north.add(title);
		this.setGridNumbersAndLetters(north, jeu);
		this.tour = new JLabel("En attente du début du jeu..", JLabel.CENTER);
		this.tour.setFont(f.deriveFont(f.getStyle() | Font.BOLD));
		this.tour.setFont(this.tour.getFont().deriveFont(16f));
		this.message = new JMessage();
		this.message.setActionListener(buttons);
		jeu.add(this.tour, BorderLayout.SOUTH);
		jeu.add(this.plateau, BorderLayout.CENTER);
		this.add(jeu);
		this.add(this.message, BorderLayout.EAST);
		this.pack();
		this.plateau.setSize(new Dimension(this.getHeight() - 50, this.getHeight()));
		this.setPreferredSize(new Dimension(700, 700));
		this.setMinimumSize(new Dimension(700, 700));
		this.dispose();

	}

	/**
	 * Met en place le menu
	 * 
	 * @param action
	 */
	private void setupMenus(ActionListener action) {
		JMenuMario menu = new JMenuMario();
		menu.setActionListener(action);
		this.setJMenuBar(this.menuBar = menu);

	}

	/**
	 * Place la grille des Lettre et des nombres pour définir la position visuelle
	 * des pions
	 * 
	 * @param lettersContainer le conteneur horizontal se base sur le panel prévu
	 *                         pour le titre initialement
	 */
	private void setGridNumbersAndLetters(JPanel lettersContainer, JPanel numberContainer) {
		GridLayout layletters = new GridLayout(1, 8), laydigits = new GridLayout(8, 1);
		JPanel letters = new JPanel(layletters);
		JPanel digits = new JPanel(laydigits);
		for (int a = 0; a < 8; a++) {
			digits.add(new JLabel(String.format(
					"<html><body style=\"text-align: justify;  text-justify: inter-word;\">%s</body></html>",
					(char) ('A' + a)), JLabel.CENTER));
			letters.add(new JLabel(String.valueOf(a), JLabel.CENTER));
		}

		lettersContainer.add(letters);
		numberContainer.add(digits, BorderLayout.WEST);
		numberContainer.add(lettersContainer, BorderLayout.NORTH);
	}

	/**
	 * s On charge l'image, peut se trouver à plusieurs endroit selon la compilation
	 */
	private void loadIcon() {
		try {
			this.setIconImage(ImageIO.read(JFenetre.class.getClassLoader().getResource("lapingoomba.png")));
		} catch (Exception e) {
			try {
				this.setIconImage(
						ImageIO.read(JFenetre.class.getClassLoader().getResource("resources/lapingoomba.png")));
			} catch (IllegalArgumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				throw e1;
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	/**
	 * ajoute la gestion du clic sur une case
	 * 
	 * @param l
	 */
	public void setCaseListener(MouseListener l) {
		for (JCase j : this.plateau.getCases()) {
			j.addMouseListener(l);
		}
	}

	/**
	 * affiche un message en bas
	 */
	public void afficherMessage(String mess) {
		this.tour.setText(mess);
	}

	/**
	 * Affiche un pion a la position indiquée en paramètres
	 */
	public void afficherPion(char lettre, int position, EquipeType joueur1, String nom, int vie, int atk, int def,
			int magie, String image) {
		this.getPlateau().setCase(lettre, position, joueur1, nom, vie, atk, def, magie, image);
	}

	/**
	 * Retourne le plateau GUI
	 * 
	 * @return
	 */
	public JPlateau getPlateau() {
		return this.plateau;
	}

	/**
	 * affiche un message en bas
	 */
	@Override
	public void afficherTourJoueur(String texte) {
		this.tour.setText(texte); // au tour du joueur X
	}

	@Override
	public void retirerPion(char lettre, int position) {
		this.getPlateau().emptyCase(lettre, position);
	}

	@Override
	public void afficherAtkCase(char lettre, int position, int atk) {
		this.getPlateau().getCase(lettre, position).setAtk(atk);
	}

	@Override
	public void afficherSubirDegats(char lettre, int position, int degats, int def, int magie, Runnable next) {
		this.getPlateau().getCase(lettre, position).setDegats(degats, def, magie, next);

	}

	@Override
	public void afficherSoins(char lettre, int position, int soin, Runnable next) {
		this.getPlateau().getCase(lettre, position).setSoin(soin, next);

	}

	@Override
	public void afficherSelection(char lettre, int position) {
		this.getPlateau().getCase(lettre, position).setSelected(true);
	}

	@Override
	public void retirerAffichageSelection(char lettre, int position) {
		this.getPlateau().getCase(lettre, position).setSelected(false);

	}

	@Override
	public void retirerMouvementsPossible() {
		for (JCase p : this.getPlateau().getCases())
			p.setHighlight(false);
	}

	@Override
	public void afficherMouvementsPossible(Collection<Move> positions) {
		for (Move p : positions)
			this.getPlateau().getCase(p.to().getLettre(), p.to().getPosition()).setHighlight(true);
	}

	@Override
	public void mourirPion(char lettre, int position, Runnable runnable) {
		this.getPlateau().getCase(lettre, position).setMourir(runnable);
	}

	/**
	 * Crée l'évenement de fermeture avec la Croix, ferme le programme
	 */
	public void close() {
		JFenetre.this.processWindowEvent(new WindowEvent(JFenetre.this, WindowEvent.WINDOW_CLOSING));
	}

	@Override
	public void afficherGainTresor(char lettre, int position, LootableType type, Runnable runnable) {
		this.getPlateau().getCase(lettre, position).setGainTresor(type, runnable);

	}

	@Override
	public void ecrireDescription(String description) {
		this.ecrireDescription(description, true);
	}

	@Override
	public void ecrireDescription(String description, boolean carriage) {
		this.ecrireDescription(description, true, "Console");
	}

	@Override
	public void ecrireDescription(String description, boolean carriage, String prefix) {
		this.message.envoyerMessage(
				"<b>[" + prefix + "]</b> " + description + (carriage ? "<br/></html>" : "") + "</html>");
	}

	public Promptable getPrompt() {
		return this.prompt;
	}

	public void message(String string) {
		this.prompt.message(string);
	}

	public JMenuBar getMenu() {
		return this.menuBar;
	}

	public String getMessage() {
		return this.message.getMessage();
	}

	public void resetMessageInput() {
		this.message.resetInput();
	}

	public void viderDesc() {
		this.message.viderDesc();
	}

	public void setEnabledInput(boolean b) {
		if (b)
			this.message.enable();
		else
			this.message.disable();
	}

}
