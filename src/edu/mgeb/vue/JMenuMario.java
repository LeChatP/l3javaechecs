package edu.mgeb.vue;

import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 * 
 * @author lechatp, Aubin BERINGUET
 *
 */
public class JMenuMario extends JMenuBar {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1908308714019368756L;
	private JMenu fichier;
	private JMenu interro;
	private JMenuItem newGame;
	private JMenuItem newOnlineGame;
	private JMenuItem joinOnlineGame;
	private JMenuItem load;
	private JMenuItem save;
	private JMenuItem saveas;
	private JMenuItem apropos;
	private JMenu online;
	private JMenuItem pause;
	private JMenuItem reprendre;

	public JMenuMario() {
		this.fichier = new JMenu("Fichier");
		this.online = new JMenu("Online");
		this.interro = new JMenu("?");

		this.newGame = this.createMenuItem("Nouvelle Partie", JAction.NEW);
		this.newOnlineGame = this.createMenuItem("Nouvelle Partie en Ligne", JAction.NEWONLINE);
		this.joinOnlineGame = this.createMenuItem("Rejoindre Partie en Ligne", JAction.JOINONLINE);
		this.pause = this.createMenuItem("Mettre en pause", JAction.PAUSE);
		this.reprendre = this.createMenuItem("Reprendre la partie", JAction.REPRENDRE);
		this.load = this.createMenuItem("Charger", JAction.LOAD);
		this.save = this.createMenuItem("Sauvegarder", JAction.SAVE);
		this.saveas = this.createMenuItem("Sauvegarder sous...", JAction.SAVEAS);
		this.apropos = this.createMenuItem("A Propos", JAction.APROPOS);

		this.interro.add(this.apropos);

		this.fichier.add(this.newGame);
		this.online.add(this.newOnlineGame);
		this.online.add(this.joinOnlineGame);
		this.fichier.add(this.load);
		this.fichier.add(this.save);
		this.fichier.add(this.saveas);

		this.online.add(this.pause);
		this.online.add(this.reprendre);

		this.add(this.fichier);
		this.add(this.online);
		this.add(this.interro);
	}

	private JMenuItem createMenuItem(String text, JAction action) {
		JMenuItem item = new JMenuItem(text);
		item.setActionCommand(action.name());
		return item;
	}

	public void setActionListener(ActionListener action) {
		this.newGame.addActionListener(action);
		this.newOnlineGame.addActionListener(action);
		this.joinOnlineGame.addActionListener(action);
		this.load.addActionListener(action);
		this.save.addActionListener(action);
		this.saveas.addActionListener(action);
		this.apropos.addActionListener(action);
		this.pause.addActionListener(action);
		this.reprendre.addActionListener(action);
	}
}
