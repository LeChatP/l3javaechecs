package edu.mgeb.vue;

import java.awt.event.MouseListener;
import java.util.Collection;

import edu.mgeb.affrontement.LootableType;
import edu.mgeb.plateau.Move;
import edu.mgeb.protagonistes.EquipeType;

/**
 * Interface permettant d'effectuer des affichages dans le GUI
 * 
 * @author lechatp, Matthieu Gallea
 */
public interface ILivre {
	public void afficherTourJoueur(String texte);

	public void afficherMessage(String texte);

	public void afficherPion(char lettre, int position, EquipeType equipeType, String nom, int vie, int atk, int def,
			int magie, String image);

	public void retirerPion(char lettre, int position);

	public void afficherAtkCase(char lettre, int position, int atk);

	public void afficherSubirDegats(char lettre, int position, int degats, int def, int magie, Runnable next);

	public void afficherSoins(char lettre, int position, int soin, Runnable next);

	public void afficherGainTresor(char lettre, int position, LootableType lootableType, Runnable next);

	public void afficherSelection(char lettre, int position);

	public void retirerAffichageSelection(char lettre, int position);

	public void afficherMouvementsPossible(Collection<Move> positions);

	public void retirerMouvementsPossible();

	public void mourirPion(char lettre, int position, Runnable object);

	public void message(String message);

	public void setCaseListener(MouseListener object);

	public void ecrireDescription(String description, boolean carriage, String prefix);

	public void ecrireDescription(String description, boolean carriage);

	public void ecrireDescription(String description);

}
