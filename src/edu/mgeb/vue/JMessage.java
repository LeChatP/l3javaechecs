package edu.mgeb.vue;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;

/**
 * 
 * @author lechatp, Aubin BERINGUET
 *
 */
public class JMessage extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1473462062766501087L;
	private JTextPane texte;
	private JTextField message;
	private JButton button;

	public JMessage() {
		this.setLayout(new BorderLayout());
		this.texte = new JTextPane();
		this.texte.setContentType("text/html");
		this.texte.setPreferredSize(new Dimension(250, 60));
		this.texte.setEditable(false);
		this.texte.setText("<html></html>");
		// this.texte.setMaximumSize(new Dimension());
		JScrollPane scroll = new JScrollPane(this.texte, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		JPanel message = new JPanel();
		message.setLayout(new BorderLayout());
		this.message = new JTextField();
		this.message.setActionCommand(JAction.SEND.name());
		message.setPreferredSize(new Dimension(150, 50));
		this.button = new JButton("▶");
		this.button.setActionCommand(JAction.SEND.name());
		message.add(this.message);
		message.add(this.button, BorderLayout.EAST);
		this.add(scroll);
		this.add(message, BorderLayout.SOUTH);
		this.disable();
	}

	public void envoyerMessage(String texte) {
		// TODO Auto-generated method stub
		String m = this.texte.getText().substring(0, this.texte.getText().length() - 16);
		this.texte.setText(m + texte);
	}

	public String getMessage() {
		return this.message.getText();
	}

	public void setActionListener(ActionListener al) {
		this.button.addActionListener(al);
		this.message.addActionListener(al);
	}

	public void resetInput() {
		this.message.setText("");

	}

	public void viderDesc() {
		this.texte.setText("<html></html>");
	}

	public void disable() {
		this.button.setEnabled(false);
		this.message.setEnabled(false);
	}

	public void enable() {
		this.button.setEnabled(true);
		this.message.setEnabled(true);
	}
}
