package edu.mgeb.vue;

import java.awt.event.MouseListener;
import java.util.Collection;

import edu.mgeb.affrontement.LootableType;
import edu.mgeb.plateau.Move;
import edu.mgeb.protagonistes.EquipeType;

/**
 * 
 * @author lechatp, Aubin BERINGUET
 *
 */
public class LivreServeur implements ILivre {

	@Override
	public void afficherTourJoueur(String texte) {
		return;
	}

	@Override
	public void afficherMessage(String texte) {
		return;
	}

	@Override
	public void afficherPion(char lettre, int position, EquipeType joueur1, String nom, int vie, int atk, int def,
			int magie, String image) {
		return;
	}

	@Override
	public void retirerPion(char lettre, int position) {
		return;
	}

	@Override
	public void afficherAtkCase(char lettre, int position, int atk) {
		return;
	}

	@Override
	public void afficherSubirDegats(char lettre, int position, int degats, int def, int magie, Runnable next) {
		if (next != null)
			next.run();
	}

	@Override
	public void afficherSoins(char lettre, int position, int soin, Runnable next) {
		if (next != null)
			next.run();
	}

	@Override
	public void afficherGainTresor(char lettre, int position, LootableType type, Runnable next) {
		if (next != null)
			next.run();
	}

	@Override
	public void afficherSelection(char lettre, int position) {
		return;
	}

	@Override
	public void retirerAffichageSelection(char lettre, int position) {
		return;
	}

	@Override
	public void afficherMouvementsPossible(Collection<Move> positions) {
		return;
	}

	@Override
	public void retirerMouvementsPossible() {
		return;
	}

	@Override
	public void mourirPion(char lettre, int position, Runnable next) {
		if (next != null)
			next.run();
	}

	@Override
	public void message(String message) {
		return;
	}

	@Override
	public void setCaseListener(MouseListener object) {
		return;
	}

	@Override
	public void ecrireDescription(String description) {
		return;
	}

	@Override
	public void ecrireDescription(String description, boolean carriage) {
		return;
	}

	@Override
	public void ecrireDescription(String description, boolean carriage, String prefix) {
		return;
	}

}
