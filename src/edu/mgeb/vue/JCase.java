package edu.mgeb.vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

import edu.mgeb.affrontement.LootableType;
import edu.mgeb.plateau.Position;
import edu.mgeb.protagonistes.EquipeType;
import jiconfont.icons.font_awesome.FontAwesome;
import jiconfont.swing.IconFontSwing;

/**
 * Panel visible d'une case
 * 
 * @author lechatp, Matthieu Gallea
 */
public class JCase extends JPanel implements ActionListener {

	private static final long serialVersionUID = -3633897651863833826L;
	public static volatile boolean inTimer;

	/**
	 * Labels de statistiques
	 */
	private JLabel jVie;
	private JLabel jAtk;
	private JLabel jDef;
	private JLabel jName;
	private JLabel jDegats;
	private JLabel jMagie;
	/**
	 * Position logique de la case
	 */
	private Position pos;
	/**
	 * Case paire ou impair pour la couleur
	 */
	private boolean pair;
	/**
	 * Timer pour les animations qui durent 1 seconde
	 */
	private Timer timer;
	/**
	 * Fonction à executer après animation
	 */
	private Runnable tmp;
	/**
	 * Couleur du texte en fonction du joueur
	 */
	private Color colortext;
	/**
	 * Image de fond
	 */
	private BufferedImage image;

	public JCase(int number, boolean pair) {
		this.pair = pair;
		this.pos = new Position((char) ('A' + number / 8), number % 8);
		this.setOpaque(true);
		this.setBg();
		BorderLayout layout = new BorderLayout();
		this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		this.setLayout(layout);
		this.jName = new JLabel("", JLabel.CENTER);
		this.jVie = new JLabel();
		this.jAtk = new JLabel("", JLabel.RIGHT);
		this.jDef = new JLabel("");
		this.jMagie = new JLabel("", JLabel.RIGHT);
		this.jDegats = new JLabel("", JLabel.CENTER);
		this.jDegats.setFont(this.jDegats.getFont().deriveFont(20f));
		this.jName.setBackground(new Color(255, 255, 255, 180));
		this.jName.setOpaque(true);
		this.jVie.setBackground(new Color(255, 255, 255, 180));
		this.jVie.setOpaque(true);
		this.jAtk.setBackground(new Color(255, 255, 255, 180));
		this.jAtk.setOpaque(true);
		this.jDef.setBackground(new Color(255, 255, 255, 180));
		this.jDef.setOpaque(true);
		this.jMagie.setBackground(new Color(255, 255, 255, 180));
		this.jMagie.setOpaque(true);
		JPanel stats = new JPanel();
		stats.setOpaque(false);
		GridLayout grid = new GridLayout(2, 2);
		stats.setLayout(grid);
		stats.add(new JAlphaContainer(this.jVie));
		stats.add(new JAlphaContainer(this.jAtk));
		stats.add(new JAlphaContainer(this.jDef));
		stats.add(new JAlphaContainer(this.jMagie));
		this.add(this.jDegats, BorderLayout.CENTER);
		this.add(this.jName, BorderLayout.NORTH);
		this.add(stats, BorderLayout.SOUTH);
		this.timer = new Timer(1000, this);
		this.timer.setRepeats(false);
		this.setVisible(true);

	}

	/**
	 * Coloration du fond
	 */
	public void setBg() {
		if (this.pair)
			this.setBackground(Color.WHITE);
		else
			this.setBackground(new Color(172, 171, 255));
	}

	/**
	 * Remplace la position dans la fenêtre par la position dans le plateau A
	 * Réusiner un jour
	 */
	@Override
	public Point getLocation() {
		return new Point(pos.getLettre(), pos.getPosition());
	}

	/**
	 * Ajout de l'image en fond, centrée
	 */
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		int x = (super.getWidth() - 131) / 2;
		int y = (super.getHeight() - 131) / 2;
		g.drawImage(this.image, x, y, null);
	}

	/**
	 * Application des statistiques d'un pion
	 * 
	 * @param name    Son nom
	 * @param atk     Son attaque
	 * @param vie     ses Points de vie
	 * @param def     Ses points de defense
	 * @param magie   Ses points de magie
	 * @param joueur1 A qui appartient le pion (coloration du texte)
	 * @param image   image a afficher
	 */
	public void setPion(String name, int atk, int vie, int def, int magie, EquipeType joueur1, String image) {
		URL url = JCase.class.getClassLoader().getResource(image);
		try {
			this.image = ImageIO.read(url);
		} catch (Exception e) {
			url = JCase.class.getClassLoader().getResource("resources/" + image);
			try {
				this.image = ImageIO.read(url);
			} catch (IOException e1) {
				System.out.println(url.getFile());
				e1.printStackTrace();
			}
		}
		this.setColor(joueur1);
		this.setName(name);
		this.setAtk(atk);
		this.setVie(vie);
		this.setDef(def);
		this.setMagie(magie);
		this.updateUI();
	}

	/**
	 * Couleur en fonction du joueur
	 * 
	 * @param joueur1
	 */
	public void setColor(EquipeType joueur1) {
		this.colortext = joueur1.estLapins() ? new Color(0, 100, 0) : Color.RED;
		this.jName.setForeground(colortext);
		this.jAtk.setForeground(colortext);
		this.jDef.setForeground(colortext);
		this.jDegats.setForeground(colortext);
		this.jMagie.setForeground(colortext);
		this.jVie.setForeground(colortext);
	}

	/**
	 * Vide la case
	 */
	public void empty() {
		this.jAtk.setText("");
		this.jAtk.setIcon(null);
		this.jAtk.revalidate();
		this.jDef.setText("");
		this.jDef.setIcon(null);
		this.jDef.revalidate();
		this.jMagie.setText("");
		this.jMagie.setIcon(null);
		this.jMagie.revalidate();
		this.jVie.setText("");
		this.jVie.setIcon(null);
		this.jVie.revalidate();
		this.emptyDegats();
		this.jName.setText("");
		this.image = null;
		this.repaint();
		this.setHighlight(false);
	}

	/**
	 * Retire les animations
	 */
	private void emptyDegats() {
		this.jDegats.setForeground(Color.WHITE);
		this.jDegats.setText("");
		this.jDegats.setIcon(null);
		this.jDegats.revalidate();
	}

	/**
	 * affiche l'attaque avec son icone
	 * 
	 * @param atk
	 */
	public void setAtk(int atk) {
		this.jAtk.setText(String.valueOf(atk));
		this.jAtk.setIcon(IconFontSwing.buildIcon(FontAwesome.CROSSHAIRS, 18, this.colortext));
		this.jAtk.setBackground(new Color(255, 255, 255, 180));
	}

	/**
	 * affiche la défense avec son icone
	 * 
	 * @param def
	 */
	public void setDef(int def) {
		this.jDef.setText(String.valueOf(def));
		this.jDef.setIcon(IconFontSwing.buildIcon(FontAwesome.SHIELD, 18, this.colortext));
		this.jDef.setBackground(new Color(255, 255, 255, 180));

	}

	/**
	 * Affiche la vie avec son icone
	 * 
	 * @param vie
	 */
	public void setVie(int vie) {
		this.jVie.setText(String.valueOf(vie));
		this.jVie.setIcon(IconFontSwing.buildIcon(FontAwesome.HEART, 18, this.colortext));
		this.jVie.setBackground(new Color(255, 255, 255, 180));
	}

	/**
	 * Affiche la magie avec son icone
	 * 
	 * @param magie
	 */
	public void setMagie(int magie) {
		this.jMagie.setText(String.valueOf(magie));
		this.jMagie.setIcon(IconFontSwing.buildIcon(FontAwesome.MAGIC, 18, this.colortext));
		this.jMagie.setBackground(new Color(255, 255, 255, 180));
	}

	/**
	 * Affiche une animation de dégât
	 * 
	 * @param degats   dégâts à afficher
	 * @param def      défense a mettre à jour
	 * @param magie    magie à mettre à jour
	 * @param runnable action a effectuer après animation
	 */
	public synchronized void setDegats(int degats, int def, int magie, Runnable runnable) {
		this.tmp = runnable;
		this.jDegats.setForeground(Color.RED);
		this.jDegats.setText("-" + String.valueOf(degats));
		int i = Integer.parseInt(this.jVie.getText());
		this.jVie.setText(String.valueOf(degats > i ? 0 : i - degats));
		this.setDef(def);
		this.setMagie(magie);
		this.timer.start();
		JCase.inTimer = true;
	}

	/**
	 * Animation de Soins
	 * 
	 * @param soin     la valeur du soin
	 * @param runnable Fonction a executer après animation
	 */
	public void setSoin(int soin, Runnable runnable) {
		this.tmp = runnable;
		this.jDegats.setForeground(Color.GREEN);
		this.jDegats.setText("+" + String.valueOf(soin));
		this.timer.start();
		JCase.inTimer = true;
	}

	/**
	 * Applique un nom à la case
	 */
	public void setName(String name) {
		this.jName.setText(String.format(
				"<html><body style=\"text-align: justify;  text-justify: inter-word;\">%s</body></html>", name));
	}

	/**
	 * Met en surbrillance la case
	 * 
	 * @param h
	 */
	public void setHighlight(boolean h) {
		if (h) {
			this.setBackground(new Color(253, 253, 4, 180));
		} else {
			this.setBg();
		}
	}

	/**
	 * Applique les bordures d'un combat
	 * 
	 * @param h
	 */
	public void setCombat(boolean h) {
		if (h)
			this.setBorder(BorderFactory.createLineBorder(Color.RED));
		else
			this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
	}

	/**
	 * Affiche les bordures en surbrillance
	 * 
	 * @param h
	 */
	public void setSelected(boolean h) {
		if (h)
			this.setBorder(BorderFactory.createLineBorder(Color.ORANGE));
		else
			this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
	}

	/**
	 * Effectue l'animation de mort
	 * 
	 * @param runnable action a executer après l'animation
	 */
	public synchronized void setMourir(Runnable runnable) {
		this.tmp = runnable;
		this.empty();
		this.setBackground(new Color(253, 4, 4, 200));
		this.jDegats.setIcon(IconFontSwing.buildIcon(FontAwesome.TIMES, 70));
		this.jDegats.revalidate();
		this.repaint();
		this.timer.start();
	}

	/**
	 * Action effectuée après animation
	 */
	@Override
	public synchronized void actionPerformed(ActionEvent arg0) {
		this.setBg();
		this.emptyDegats(); // on réinitialise l'animation
		this.timer.stop();
		if (this.tmp != null) {
			tmp.run(); // on appelle la fonction a faire après animation
		}
		JCase.inTimer = false; // on retire le blocage des actions.
	}

	/**
	 * On effectue la'nimation de gain d'un trésor
	 * 
	 * @param type     Type du gain
	 * @param runnable Fonction à exécuter après animation
	 */
	public void setGainTresor(LootableType type, Runnable runnable) {
		this.tmp = runnable;
		this.jDegats.setForeground(Color.cyan);
		switch (type) {
		case EPEE:
		case ARC:
		case LANCEPIERRE:
			this.jDegats.setIcon(IconFontSwing.buildIcon(FontAwesome.CROSSHAIRS, 70));
			break;
		case EAU:
		case FEU:
		case AIR:
		case TERRE:
			this.jDegats.setIcon(IconFontSwing.buildIcon(FontAwesome.MAGIC, 70));
			break;
		default:
			this.jDegats.setIcon(IconFontSwing.buildIcon(FontAwesome.SHIELD, 70));
			break;
		}
		this.timer.start();
		JCase.inTimer = true;
	}

}
