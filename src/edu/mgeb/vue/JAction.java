package edu.mgeb.vue;

/**
 * Type d'action dans le menu du GUI
 * 
 * @author lechatp, Matthieu Gallea, Aubin BERINGUET
 */
public enum JAction {
	SAVE, SAVEAS, LOAD, APROPOS, EXIT, NEW, NEWONLINE, JOINONLINE, SEND, PAUSE, REPRENDRE;
}