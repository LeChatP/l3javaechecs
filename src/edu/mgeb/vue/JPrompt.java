package edu.mgeb.vue;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Locale;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import edu.mgeb.commun.JoinAddress;

public class JPrompt implements Promptable {

	private JFileChooser chooser;

	/**
	 * Initialise le sélectionneur de fichier
	 */
	public JPrompt() {
		this.chooser = new JFileChooser() {
			private static final long serialVersionUID = 1L;

			@Override
			public void approveSelection() {
				File f = getSelectedFile();
				if (f.exists() && getDialogType() == SAVE_DIALOG) {
					int result = JOptionPane.showConfirmDialog(this, "Le fichier existe, le remplacer?",
							"Fichier déjà existant", JOptionPane.YES_NO_CANCEL_OPTION);
					switch (result) {
					case JOptionPane.YES_OPTION:
						super.approveSelection();
						return;
					case JOptionPane.NO_OPTION:
						return;
					case JOptionPane.CLOSED_OPTION:
						return;
					case JOptionPane.CANCEL_OPTION:
						cancelSelection();
						return;
					}
				}
				super.approveSelection();
			}
		};
		chooser.setLocale(Locale.FRANCE);
	}

	@Override
	public String chooseFile(File defaultFile) {
		chooser.setApproveButtonText("Charger");
		chooser.setDialogTitle("Choississez un fichier à Charger");
		chooser.setCurrentDirectory(defaultFile.getParentFile());
		chooser.setSelectedFile(defaultFile);
		int returnVal = chooser.showOpenDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			return chooser.getSelectedFile().getName();
		}
		return null;
	}

	@Override
	public String saveFile(File defaultFile) {
		chooser.setApproveButtonText("Sauvegarder");
		chooser.setDialogTitle("Choississez un fichier à Sauvegarder");
		chooser.setCurrentDirectory(defaultFile.getParentFile());
		chooser.setSelectedFile(defaultFile);
		int returnVal = chooser.showSaveDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			return chooser.getSelectedFile().getName();
		}
		return null;
	}

	@Override
	public void message(String message) {
		Object[] options = { "OK" };
		JOptionPane.showOptionDialog(null, message, "Message", JOptionPane.PLAIN_MESSAGE,
				JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);
	}

	@Override
	public void error(String message) {
		Object[] options = { "OK" };
		JOptionPane.showOptionDialog(null, message, "Erreur", JOptionPane.PLAIN_MESSAGE, JOptionPane.ERROR_MESSAGE,
				null, options, options[0]);
	}

	@Override
	public JoinAddress askJoinAddress() {
		return new JConnectInput().showFullDialog();
	}

	@Override
	public InetAddress askAddress() {
		while (true)
			try {
				String addr = JOptionPane.showInputDialog("Entrez l'adresse du serveur", "127.0.0.1");
				if (addr == null)
					return null;
				return InetAddress.getByName(addr);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

}
