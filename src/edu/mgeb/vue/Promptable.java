package edu.mgeb.vue;

import java.io.File;
import java.net.InetAddress;

import edu.mgeb.commun.JoinAddress;

/**
 * Interface pour tout affichage de message ou demande d'information à
 * l'utilisateur.
 * 
 * @author lechatp
 *
 */
public interface Promptable {
	public void message(String string);

	public String chooseFile(File defaultFile);

	public void error(String message);

	public String saveFile(File defaultFile);

	public InetAddress askAddress();

	public JoinAddress askJoinAddress();
}
