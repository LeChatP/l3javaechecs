package edu.mgeb.client;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import edu.mgeb.affrontement.Resume;
import edu.mgeb.commun.CommunicationClient;
import edu.mgeb.controleur.Jeu;
import edu.mgeb.plateau.Move;
import edu.mgeb.plateau.Plateau;
import edu.mgeb.plateau.Tour;
import edu.mgeb.protagonistes.EquipeType;

/**
 * 
 * @author lechatp, Aubin BERINGUET
 *
 */
public class CommunicationClientImpl extends UnicastRemoteObject implements CommunicationClient {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2206923270551789407L;
	private Jeu jeu;

	public CommunicationClientImpl(Jeu jeu) throws RemoteException {
		super();
		this.jeu = jeu;
	}

	@Override
	public boolean commencerPartie(EquipeType role, EquipeType commence) throws RemoteException {
		jeu.newGame(role, commence);
		return true;
	}

	@Override
	public void sendMove(Move move, Resume resume) throws RemoteException {
		this.jeu.remoteMouvement(move, resume);
	}

	@Override
	public void terminerPartie(EquipeType gagnant) throws RemoteException {
		this.jeu.finDuJeu(gagnant);
	}

	@Override
	public void sendMessage(EquipeType equipe, String message) throws RemoteException {
		this.jeu.recvAdvMessage(equipe, message);

	}

	@Override
	public void partieEnPause(EquipeType equipe) throws RemoteException {
		this.jeu.pause(equipe);
	}

	@Override
	public void resume() throws RemoteException {
		this.jeu.reprendre();
	}

	@Override
	public void resume(Plateau plateau, Tour tour) throws RemoteException {
		this.jeu.reprendre(plateau, tour);
	}

}
