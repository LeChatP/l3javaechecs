package edu.mgeb.armure;

import edu.mgeb.affrontement.LootableType;
import edu.mgeb.affrontement.Rarete;

/**
 * Classe définissant la pièce d'armure des jambières
 * 
 * @author lechatp, Matthieu Gallea
 */
public class Jambieres extends Armure {
	/**
	 * 
	 */
	private static final long serialVersionUID = 537611359449176700L;

	public Jambieres(Rarete rarete) {
		super("Jambières", 10, rarete, true);
	}

	@Override
	public LootableType getLootableType() {
		return LootableType.JAMBIERES;
	}
}
