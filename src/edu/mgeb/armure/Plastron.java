package edu.mgeb.armure;

import edu.mgeb.affrontement.LootableType;
import edu.mgeb.affrontement.Rarete;

/**
 * Classe définissant la pièce d'armure du plastron
 * 
 * @author lechatp, Matthieu Gallea
 */
public class Plastron extends Armure {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3665433333858140712L;

	public Plastron(Rarete rarete) {
		super("Pastron", 15, rarete, false);
	}

	@Override
	public LootableType getLootableType() {
		return LootableType.PLASTRON;
	}
}
