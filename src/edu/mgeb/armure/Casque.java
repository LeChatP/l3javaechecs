package edu.mgeb.armure;

import edu.mgeb.affrontement.LootableType;
import edu.mgeb.affrontement.Rarete;

/**
 * Classe définissant la pièce d'armure du casque
 * 
 * @author lechatp, Matthieu Gallea
 */
public class Casque extends Armure {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6480315782291488727L;

	public Casque(Rarete rarete) {
		super("Casque", 5, rarete, false);
	}

	@Override
	public LootableType getLootableType() {
		return LootableType.CASQUE;
	}
}
