package edu.mgeb.armure;

import edu.mgeb.affrontement.Lootable;
import edu.mgeb.affrontement.Rarete;
import edu.mgeb.vue.ILivre;

/**
 * Classe généralisant les pièces d'armures
 * 
 * @author lechatp, Matthieu Gallea
 */
public abstract class Armure implements Lootable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5476279461846958495L;
	protected int resistance;
	protected String nature;
	private boolean plural;

	/**
	 * Constructeur générique
	 * 
	 * @param resistance
	 * @param rarete
	 */
	public Armure(String nature, int resistance, Rarete rarete, boolean plural) {
		this.nature = nature;
		this.resistance = resistance;
		this.resistance *= rarete.getMultiplicateur();
		this.plural = plural;
	}

	/**
	 * Encaise la résistance, se brise une fois encaissé un maximum de dégâts
	 * 
	 * @param degat
	 * @return les dégats qui ne sont pas encaissés, 0 si tout encaissé
	 */
	public int degatResistance(ILivre livre, int degat) {
		this.resistance -= degat;
		if (this.resistance < 0) {
			livre.ecrireDescription(" " + this.nature + " se brise.");
			int returned = Math.abs(this.resistance);
			this.resistance = 0;
			return returned;
		} else {
			livre.ecrireDescription(" " + this.nature + " encaisse le restant des dégâts.");
			return 0;
		}
	}

	/**
	 * Résistance de l'armure
	 * 
	 * @return
	 */
	public int getResistance() {
		return resistance;
	}

	/**
	 * Fonction de hachage pour le stockage dans les HashSet
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nature == null) ? 0 : nature.hashCode());
		return result;
	}

	/**
	 * Fonction d'égalité pour le stockage dans les HashSet
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Armure other = (Armure) obj;
		if (nature == null) {
			if (other.nature != null)
				return false;
		} else if (!nature.equals(other.nature))
			return false;
		return true;
	}

	public String getNature() {
		return this.nature;
	}

	public String getPluralPrefix() {
		return this.plural ? "Ses" : "Son";
	}

}
