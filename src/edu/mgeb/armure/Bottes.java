package edu.mgeb.armure;

import edu.mgeb.affrontement.LootableType;
import edu.mgeb.affrontement.Rarete;

/**
 * Classe définissant la pièce d'armure des bottes
 * 
 * @author lechatp, Matthieu Gallea
 */
public class Bottes extends Armure {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8342422028751459196L;

	public Bottes(Rarete rarete) {
		super("Bottes", 5, rarete, true);
	}

	@Override
	public LootableType getLootableType() {
		return LootableType.BOTTES;
	}
}
