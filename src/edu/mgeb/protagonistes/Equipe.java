package edu.mgeb.protagonistes;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;

/**
 * Classe d'une Equipe d'être vivants
 * 
 * @author lechatp, Matthieu Gallea
 */
public class Equipe implements Serializable {
	private static final long serialVersionUID = -3933802688654258082L;
	/**
	 * Liste des êtrevivants d'une Equipe
	 */
	private List<EtreVivant> list;
	/**
	 * Factory permettant simplement de créer des EtreVivant
	 */
	private transient EtreVivantFactory factory;

	/**
	 * Crée tous les êtres vivants d'une équipe en définisant son propriétaire j1
	 * 
	 * @param j1
	 */
	public Equipe(EquipeType j1) {
		this.factory = new EtreVivantFactory(j1);
		this.list = new LinkedList<>();
		this.list.add(this.factory.creerEtre(TypeEnrolable.TOAD));
		this.list.add(this.factory.creerEtre(TypeEnrolable.YOSHI));
		this.list.add(this.factory.creerEtre(TypeEnrolable.KOOPA));
		this.list.add(this.factory.creerEtre(TypeEnrolable.PEACH));
		this.list.add(this.factory.creerEtre(TypeEnrolable.MARIO));
		this.list.add(this.factory.creerEtre(TypeEnrolable.KOOPA));
		this.list.add(this.factory.creerEtre(TypeEnrolable.YOSHI));
		this.list.add(this.factory.creerEtre(TypeEnrolable.TOAD));
		for (int i = 0; i < 8; i++) {
			this.list.add(this.factory.creerEtre(TypeEnrolable.GOOMBA));
		}
		if (j1.estMario())
			Collections.reverse(this.list);
	}

	/**
	 * Focntion statique pour retrouver l'équipe à qui appartient l'etreVivant
	 * 
	 * @param e l'etre vivant
	 * @return le joueur à qui appartient l'equipe
	 */
	public static EquipeType findEquipe(EtreVivant e) {
		if (e instanceof Lapin)
			return EquipeType.LAPINS;
		else
			return EquipeType.MARIO;
	}

	/**
	 * Renvoie la liste des etresvivant de l'équipe
	 * 
	 * @return
	 */
	public List<EtreVivant> getList() {
		return list;
	}

	/**
	 * Vérifie que le type d'êtrevivant est dans la liste
	 * 
	 * @param type type de l'erevivant a tester son existence
	 * @return vrai si le type d'êtrevivant existe dans la liste, faux sinon
	 */
	public boolean exists(TypeEnrolable type) {
		for (EtreVivant etre : this.list) {
			if (etre.getTypeEnrolable() == type) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Retire l'être vivant de la liste
	 * 
	 * @param etrevivant
	 */
	public void remove(EtreVivant etrevivant) {
		Predicate<EtreVivant> condition = etre -> etre == etrevivant; // on retire si son pointeur est le même
		this.list.removeIf(condition); // on utilise les prédicats pour simplifier l'action
	}
}
