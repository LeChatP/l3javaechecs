package edu.mgeb.protagonistes;

/**
 * Définit les roles possible du jeu d'echecs
 * 
 * @author lechatp, Matthieu Gallea
 *
 */
public enum TypeEnrolable {
	MARIO, PEACH, KOOPA, YOSHI, TOAD, GOOMBA;
	// Mario - Reine
	// Peach - Roi
	// Koopa - Fou
	// Yoshi - Cavalier
	// Toad - Tour
	// Goomba - Pion
}
