package edu.mgeb.protagonistes;

import java.util.LinkedHashSet;

import edu.mgeb.plateau.Move;
import edu.mgeb.plateau.Plateau;
import edu.mgeb.plateau.Position;

/**
 * Classe encapsulant un être vivant pour copier tous ses attributs, Il définit
 * les pièces du deuxième joueur
 * 
 * @author lechatp, Matthieu Gallea
 */
public class Lapin extends EtreVivant {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6692524012713143616L;
	/**
	 * Un lapin copie le rôle qu'on lui donne
	 */
	private EtreVivant role;

	public Lapin(Enrolable role) {
		super("Le lapin " + ((EtreVivant) role).getNom(), ((EtreVivant) role).getVie(),
				((EtreVivant) role).getAttaque());
		this.role = ((EtreVivant) role);
	}

	/**
	 * @return Renvoie le role type qu'il joue
	 */
	public TypeEnrolable getTypeEnrolable() {
		return ((Enrolable) this.role).getTypeEnrolable();
	}

	/**
	 * Applique sa position initiale si son rôle est un Goomba
	 * 
	 * @param pos position initiale
	 * @return
	 */
	public boolean setInitial(Position pos) {
		try {
			((Goomba) this.role).setInitial(pos);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * @deprecated Plus utilisée depuis le GUI
	 * @see EtreVivant
	 */
	@Deprecated
	@Override
	public String mourir() {
		return "BRAAAWWW " + role.mourir();
	}

	/**
	 * Copie les déplacements du rôle
	 */
	@Override
	public LinkedHashSet<Move> listeDeplacementsPossible(Plateau p, EquipeType j1, char lettre, int position) {
		return this.role.listeDeplacementsPossible(p, j1, lettre, position);
	}

	/**
	 * @return l'image du lapin copiant le rôle qu'il joue
	 */
	@Override
	public String getImage() {
		return "lapin" + this.role.getTypeEnrolable().name().toLowerCase() + ".png";
	}

}
