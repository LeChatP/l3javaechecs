package edu.mgeb.protagonistes;

import java.util.LinkedHashSet;

import edu.mgeb.plateau.Case;
import edu.mgeb.plateau.Move;
import edu.mgeb.plateau.Plateau;
import edu.mgeb.plateau.Position;

/**
 * Classe copiant les caractéristiques du roi aux echecs Ne permet pas
 * d'effectuer le mouvement Roque aux echecs
 * 
 * @author lechatp, Matthieu Gallea
 */
public class Peach extends EtreVivant implements Enrolable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5173384828987196712L;

	public Peach() {
		super("Peach", 20, 500);

	}

	/**
	 * @deprecated
	 * @see EtreVivant
	 */
	@Deprecated
	@Override
	public String mourir() {
		return "My kingdom is lost!";
	}

	@Override
	public TypeEnrolable getTypeEnrolable() {
		return TypeEnrolable.PEACH;
	}

	/**
	 * Récupère tous les mouvements des pions adversaires pour savoir les positions
	 * où il peut se déplacer.
	 */
	@Override
	public LinkedHashSet<Move> listeDeplacementsPossible(Plateau p, EquipeType j1, char lettre, int position) {
		Position pos = new Position(lettre, position);
		LinkedHashSet<Move> allenemymoves = new LinkedHashSet<>();
		// pour connaitre ses mouvements il doit connaitre tous les autres
		for (Case e : p.getCases()) {
			if (p.estOccupe(e.getPosition()) && p.isEnemy(e.getPosition(), j1)) {
				if (p.getPion(e.getPosition()).getTypeEnrolable() == TypeEnrolable.PEACH)
					continue; // si le mouvement est un roi, on évite la boucle infinie
				allenemymoves.addAll(p.getPion(e.getPosition()).listeDeplacementsPossible(p, j1.adversaire(),
						e.getPosition().getLettre(), e.getPosition().getPosition()));
			}

		}
		LinkedHashSet<Move> possibilities = new LinkedHashSet<>();
		for (int i = -1; i < 2; i++) // tous les mouvement autour du pion
			for (int j = -1; j < 2; j++) {
				try {
					Position move = new Position((char) (lettre + i), position + j);
					if (p.isEnemy(move, j1) || !p.estOccupe(move))
						possibilities.add(new Move(pos, move));
				} catch (IllegalArgumentException aie) {
					// impossible move
					continue;
				}
			}
		possibilities.removeAll(allenemymoves); // on retire des mouvements du roi ceux possible par les autres
		// reste donc que les mouvements que le roi peut faire sans être en """echec"""
		return possibilities;
	}

	@Override
	public String getImage() {
		return "peach.png";
	}

}
