package edu.mgeb.protagonistes;

import java.util.LinkedHashSet;

import edu.mgeb.plateau.Move;
import edu.mgeb.plateau.Plateau;

/**
 * Classe pour définir que l'objet est déplaçable dans un plateau
 * 
 * @author lechatp, Matthieu Gallea
 */
public interface Movable {
	public abstract LinkedHashSet<Move> listeDeplacementsPossible(Plateau p, EquipeType equipeType, char lettre,
			int position);
}
