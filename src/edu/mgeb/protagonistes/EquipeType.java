package edu.mgeb.protagonistes;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * 
 * @author lechatp
 *
 */
public enum EquipeType {

	MARIO, LAPINS;

	private static final List<EquipeType> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
	private static final int SIZE = VALUES.size();
	private static final Random RANDOM = new Random();

	/**
	 * get Equipe adverse
	 * 
	 * @return l'equipe adverse de l'instance
	 */
	public EquipeType adversaire() {
		return this.compareTo(MARIO) == 0 ? LAPINS : MARIO;
	}

	public boolean estMario() {
		return this.compareTo(MARIO) == 0;
	}

	public boolean estLapins() {
		return this.compareTo(LAPINS) == 0;
	}

	public static EquipeType randomEquipe() {
		return VALUES.get(RANDOM.nextInt(SIZE));
	}

	@Override
	public String toString() {
		return this.name().charAt(0) + this.name().toLowerCase().substring(1);
	}
}
