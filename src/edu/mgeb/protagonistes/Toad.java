package edu.mgeb.protagonistes;

import java.util.LinkedHashSet;

import edu.mgeb.controleur.ControleurDeplacement;
import edu.mgeb.plateau.Move;
import edu.mgeb.plateau.Plateau;
import edu.mgeb.plateau.Position;

/**
 * Définit les caractéristiques d'une tour aux jeu d'echecs
 * 
 * @author lechatp, Matthieu Gallea
 */
public class Toad extends EtreVivant implements Enrolable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5058577386792997069L;

	public Toad() {
		super("Toad", 80, 10);
	}

	/**
	 * @deprecated
	 * @see EtreVivant
	 */
	@Deprecated
	@Override
	public String mourir() {
		return "I made everything for my kingdom";
	}

	/**
	 * Type Toad
	 */
	@Override
	public TypeEnrolable getTypeEnrolable() {
		return TypeEnrolable.TOAD;
	}

	/**
	 * Obtient les lignes en mouvement de la tour Ne donne pas le mouvement Roque du
	 * jeu d'echecs
	 */
	@Override
	public LinkedHashSet<Move> listeDeplacementsPossible(Plateau p, EquipeType j1, char lettre, int position) {
		Position pos = new Position(lettre, position);
		ControleurDeplacement cd = new ControleurDeplacement();
		cd.line(p, pos, j1, 1, 0);
		cd.line(p, pos, j1, -1, 0);
		cd.line(p, pos, j1, 0, 1);
		cd.line(p, pos, j1, 0, -1);
		return cd.getDeplacements();
	}

	@Override
	public String getImage() {
		return "toad.png";
	}

}
