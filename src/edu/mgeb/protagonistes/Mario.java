package edu.mgeb.protagonistes;

import java.util.LinkedHashSet;

import edu.mgeb.controleur.ControleurDeplacement;
import edu.mgeb.plateau.Move;
import edu.mgeb.plateau.Plateau;
import edu.mgeb.plateau.Position;

/**
 * Classe définissant la reine aux jeu d'echecs Une reine peut se déplacer
 * partout comme mario.
 * 
 * @author lechatp, Matthieu Gallea
 */
public class Mario extends EtreVivant implements Enrolable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1902320668413719480L;

	public Mario() {
		super("Mario", 60, 20);
	}

	/**
	 * @deprecated
	 * @see EtreVivant
	 */
	@Deprecated
	@Override
	public String mourir() {
		return "Oh no!";
	}

	@Override
	public TypeEnrolable getTypeEnrolable() {
		return TypeEnrolable.MARIO;
	}

	/**
	 * @return Renvoie toutes les lignes et diagonales que la reine peut effectuer.
	 */
	@Override
	public LinkedHashSet<Move> listeDeplacementsPossible(Plateau p, EquipeType j1, char lettre, int position) {
		Position pos = new Position(lettre, position);
		ControleurDeplacement cp = new ControleurDeplacement();
		cp.line(p, pos, j1, 1, 1); // diagonale NORD EST
		cp.line(p, pos, j1, -1, 1); // diagonale SUD EST
		cp.line(p, pos, j1, 1, -1); // diagonale NORD OUEST
		cp.line(p, pos, j1, -1, -1); // diagonale SUD OUEST
		cp.line(p, pos, j1, -1, 0); // ligne OUEST
		cp.line(p, pos, j1, 1, 0); // ligne EST
		cp.line(p, pos, j1, 0, 1); // ligne NORD
		cp.line(p, pos, j1, 0, -1); // ligne SUD

		return cp.getDeplacements();
	}

	@Override
	public String getImage() {
		return "mario.png";
	}
}
