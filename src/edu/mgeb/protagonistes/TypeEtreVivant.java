package edu.mgeb.protagonistes;

/**
 * Définit les type d'être vivant, soit un lapin, soit un réel personnage de
 * mario
 * 
 * @author lechatp, Matthieu Gallea
 */
public enum TypeEtreVivant {
	LAPIN, PERSONNAGE;
}
