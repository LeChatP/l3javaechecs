package edu.mgeb.protagonistes;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.function.Predicate;

import edu.mgeb.armes.Arme;
import edu.mgeb.armure.Armure;
import edu.mgeb.magie.Sortilege;
import edu.mgeb.vue.ILivre;

/**
 * Classe définissant un être vivant
 * 
 * @author lechatp, Matthieu Gallea
 *
 */
public abstract class EtreVivant implements Serializable, Movable {
	private static final long serialVersionUID = -3345920233862150458L;
	/**
	 * nom de l'être
	 */
	protected String nom;
	/**
	 * Vie permanente de l'être vivant
	 */
	protected final int vie;
	/**
	 * Vie de l'être vivant dans un combat
	 */
	protected int tempvie;
	/**
	 * dégat de base de l'être
	 */
	protected int attaque;
	/**
	 * L'arme de l'être si possédé
	 */
	protected Arme arme;
	/**
	 * Set d'armure de l'être
	 */
	protected Set<Armure> armures;
	/**
	 * Sort de l'être si possédé
	 */
	protected Sortilege sort;

	/**
	 * Définition d'un etre vivant par :
	 * 
	 * @param nom son nom
	 * @param vie sa vie
	 * @param atk son attaque de base
	 */
	public EtreVivant(String nom, int vie, int atk) {
		this.nom = nom;
		this.vie = vie;
		this.tempvie = vie;
		this.attaque = atk;
		this.arme = null;
		this.sort = null;
		this.armures = new HashSet<>();
	}

	/**
	 * 
	 * @return le nom de l'être
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * 
	 * @return la vie de l'être
	 */
	public int getVie() {
		return tempvie;
	}

	/**
	 * 
	 * @return l'attaque de l'être avec les dégats de l'arme
	 */
	public int getAttaque() {
		return attaque + (this.arme != null ? this.arme.getDegats() : 0);
	}

	/**
	 * 
	 * @return le rôle de l'êtrevivant
	 */
	public abstract TypeEnrolable getTypeEnrolable();

	/**
	 * Points de défense de l'armure portée
	 * 
	 * @return 0 si pas d'armure, autrement le cumul de toutes les pièces d'armures
	 *         portées
	 */
	public int getDefense() {
		int result = 0;
		for (Armure armure : this.armures)
			result += armure.getResistance();
		return result;
	}

	/**
	 * Dégâts magiques que porte l'être
	 * 
	 * @return 0 si ne possède pas de sortilège, autremeent renvoie les dégâts
	 *         magique du sort
	 */
	public int getMagie() {
		return this.sort != null ? this.sort.getDegats() : 0;
	}

	/**
	 * @return la chaine de caractères quand l'être meurt
	 */
	public abstract String mourir();

	/**
	 * Retourne le nom du fichier de l'image associé à l'être
	 * 
	 * @return nom de l'image
	 */
	public abstract String getImage();

	/**
	 * Reçoit les dégâts d'une attaque
	 * 
	 * @param forceAttaque attque reçue
	 * @param sort2        sort reçu peut être null
	 * @return les dégâts réduits
	 */
	public int subirAttaque(ILivre livre, int forceAttaque, Sortilege sort2) {
		if (!this.armures.isEmpty()) {
			Iterator<Armure> it = this.armures.iterator();
			while (forceAttaque > 0 && it.hasNext()) {
				forceAttaque = it.next().degatResistance(livre, forceAttaque);
			}
			Predicate<Armure> condition = armure -> armure.getResistance() == 0;
			this.armures.removeIf(condition);
		}
		if (sort2 != null) { // si on attaque avec un sort
			if (this.sort != null) {// si on possède un sort, on tente de réduire les dégâts
				forceAttaque += this.sort.encaisser(livre, sort2.getType(), sort2.getDegats()); // on encaisse l'attaque
																								// si faiblesse
				this.sort = null; // on perds notre sortilège
			} else { // sinon on prends tous les points d'attaque
				forceAttaque += sort2.getDegats();
			}
		}

		tempvie -= forceAttaque;
		if (vie <= 0) {
			livre.ecrireDescription(this.mourir());
		}

		return forceAttaque;

	}

	/**
	 * Réinitialise les dégâts d'un combat
	 */
	public void resetVie() {
		this.tempvie = vie;
	}

	/**
	 * L'être est défini par son nom
	 */
	@Override
	public String toString() {
		return this.nom;
	}

	/**
	 * Action de combattre
	 * 
	 * @param etre l'être à attaquer
	 * @return les dégâts effectués
	 */
	public int combattre(ILivre livre, EtreVivant etre) {
		int ret = 0;
		this.debutAttaque(livre);
		if (this.arme == null) {
			ret = etre.subirAttaque(livre, this.attaque, this.sort);
		} else {
			ret = etre.subirAttaque(livre, this.attaque + arme.attaque(livre), this.sort);
		}
		if (this.sort != null)
			livre.ecrireDescription(this.nom + " perds son sortilège");
		this.sort = null; // le sort a été utilisé.
		return ret;
	}

	private void debutAttaque(ILivre livre) {
		// on écrit avec la bonne ponctuation le texte
		String equipementsDescription = this.getEquipementAttaqueDescription();
		livre.ecrireDescription(this.nom + equipementsDescription + " attaque avec sa force de " + this.attaque); // on
																													// ne
																													// termine
																													// pas
																													// la
																													// ligne
																													// car
																													// il
																													// manque
																													// la
																													// force
																													// et
																													// les
																													// armes
	}

	private String getEquipementAttaqueDescription() {
		return this.sort != null ? " lance un " + this.sort.getDescription() + // si un sort alors on affiche
																				// description
				(this.arme != null ? ", " + arme.verbeAttaque() + " avec son " + arme.getDescription() : "") + " et" : // si
																														// une
																														// arme
																														// avec
																														// le
																														// sort
																														// on
																														// affiche
																														// l'arme
																														// avec
				this.arme != null ? " " + arme.verbeAttaque() + " avec son " + arme.getDescription() : // si pas de sort
																										// mais une arme
																										// alors on
																										// affiche
																										// uniquement
																										// l'arme
						""; // si pas d'arme si sort alors rien
	}

	/**
	 * Attribue une arme
	 * 
	 * @param arme l'arme à attribuer
	 */
	public void ajouterArme(ILivre livre, Arme arme) {
		if (this.arme != null) {
			livre.ecrireDescription(this.nom + " laisse tomber son " + this.arme.getDescription());
		}
		this.arme = arme;
		livre.ecrireDescription(this.nom + " récupère " + this.arme.getDescription() + " et devient plus puissant");
	}

	/**
	 * Attribue une armure
	 * 
	 * @param armure a attribuer
	 */
	public void ajouterArmure(ILivre livre, Armure armure) {
		if (!this.armures.add(armure)) {
			if (this.armures.remove(armure)) {
				livre.ecrireDescription(this.nom + " laisse tomber sa précédente pièce d'armure.");
			}
			this.armures.add(armure);
		}
		livre.ecrireDescription(
				this.nom + " s'équipe de " + armure.getNature() + " avec " + armure.getResistance() + " de résistance");
	}

	/**
	 * La vie de base de l'être
	 * 
	 * @return
	 */
	public int getDefaultVie() {
		return this.vie;
	}

	/**
	 * Donne un sortilège
	 * 
	 * @param loot sortilège
	 */
	public void ajouterSortilege(ILivre livre, Sortilege loot) {
		this.sort = loot;
		livre.ecrireDescription(this.nom + " possède désormais une résistance et un effet d'attaque de type "
				+ loot.getType().toString());
	}
}
