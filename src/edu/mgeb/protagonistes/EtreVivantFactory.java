package edu.mgeb.protagonistes;

/**
 * Classe permettant de construire facilement tous les être vivants
 * 
 * @author lechatp, Matthieu Gallea
 */
public class EtreVivantFactory {
	private EquipeType j1;

	/**
	 * Définit pour quel joueur l'usine est crée
	 * 
	 * @param j12
	 */
	public EtreVivantFactory(EquipeType j12) {
		this.j1 = j12;
	}

	/**
	 * Choisit la classe à instancier en fonction du type
	 * 
	 * @param e
	 * @return l'êtrevivant
	 */
	public EtreVivant creerEtre(TypeEnrolable e) {
		switch (e) {
		case GOOMBA:
			return this.instanciateClass(new Goomba());
		case KOOPA:
			return this.instanciateClass(new Koopa());
		case MARIO:
			return this.instanciateClass(new Mario());
		case PEACH:
			return this.instanciateClass(new Peach());
		case TOAD:
			return this.instanciateClass(new Toad());
		case YOSHI:
			return this.instanciateClass(new Yoshi());
		}
		throw new IllegalArgumentException("This type doesn't exist");
	}

	/**
	 * Encapsule l'enrolable en être vivant ou dans un lapin en fonction du joueur
	 * 
	 * @param e Enrolable
	 * @return un EtreVivant, ou le le lapin contenant l'êtrevivant instancié
	 */
	private EtreVivant instanciateClass(Enrolable e) {
		return j1.compareTo(EquipeType.MARIO) == 0 ? (EtreVivant) e : new Lapin(e);
	}
}
