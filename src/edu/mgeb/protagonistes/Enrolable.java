package edu.mgeb.protagonistes;

/**
 * Interface des classes êtant un rôle
 * 
 * @author lechatp, Matthieu Gallea
 *
 */
public interface Enrolable {
	/**
	 * Override la fonction dans EtreVivant
	 * 
	 * @see EtreVivant
	 * @return le role de l'êtrevivant
	 */
	public TypeEnrolable getTypeEnrolable();
}