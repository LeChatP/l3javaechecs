package edu.mgeb.protagonistes;

import java.util.LinkedHashSet;

import edu.mgeb.plateau.Move;
import edu.mgeb.plateau.Plateau;
import edu.mgeb.plateau.Position;

/**
 * Classe D'un pion dans les echecs. Ne se transforme pas en Reine une fois
 * arrivé au bout du terrain
 * 
 * @author lechatp, Matthieu Gallea
 */
public class Goomba extends EtreVivant implements Enrolable {

	private static final long serialVersionUID = -7260183499004817998L;
	private Position initial;

	/**
	 * Constructeur définissant le Pion des echecs
	 */
	public Goomba() {
		super("Goomba", 60, 10);
		this.initial = null;
	}

	public Goomba(Position initial) {
		super("Goomba", 60, 10);
		this.initial = initial;
	}

	public void setInitial(Position initial) {
		this.initial = initial;
	}

	/**
	 * @deprecated
	 * @see EtreVivant
	 */
	@Deprecated
	@Override
	public String mourir() {
		return "I die";
	}

	@Override
	public TypeEnrolable getTypeEnrolable() {
		return TypeEnrolable.GOOMBA;
	}

	/**
	 * Il peut se déplacer vers l'avant de 2 cases initialement, puis 1 case en
	 * avant une fois initié. Lorsqu'un adversaire se présente à ses diagonales
	 * avant il peut les attaquer Comportement type d'un pion aux échecs
	 */
	@Override
	public LinkedHashSet<Move> listeDeplacementsPossible(Plateau p, EquipeType j1, char lettre, int position) {
		LinkedHashSet<Move> result = new LinkedHashSet<>();
		Position pos = new Position(lettre, position);
		char l = lettre;
		if (j1.estLapins())
			l += 1;
		else
			l -= 1;
		Position avancer = null;
		try {
			avancer = new Position(l, position);
			if (!p.estOccupe(avancer)) { // avancer 1 case si pas occupé
				result.add(new Move(pos, avancer));
				avancer = new Position((char) (l + (j1.estLapins() ? +1 : -1)), position);
				if (pos.equals(this.initial) && !p.estOccupe(avancer)) // avancer 2 case si position initiale
					result.add(new Move(pos, avancer));
			}
		} catch (IllegalArgumentException iae) {
			// Case en dehors des limites.
		}

		try {
			Position diagonal = new Position(l, position + 1);
			if (p.estOccupe(diagonal) && p.isEnemy(diagonal, j1))
				result.add(new Move(pos, diagonal));
		} catch (IllegalArgumentException iae) {
			// Case en dehors des limites.
		}
		try {
			Position diagonal = new Position(l, position - 1);
			if (p.estOccupe(diagonal) && p.isEnemy(diagonal, j1))
				result.add(new Move(pos, diagonal));
		} catch (IllegalArgumentException iae) {
			// case en dehors des limites
		}

		return result;
	}

	@Override
	public String getImage() {
		return "goomba.png";
	}

}
