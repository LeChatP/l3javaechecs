package edu.mgeb.protagonistes;

import java.util.LinkedHashSet;

import edu.mgeb.plateau.Move;
import edu.mgeb.plateau.Plateau;
import edu.mgeb.plateau.Position;

/**
 * Classe définissant le cavalier dans le jeu des echecs
 * 
 * @author lechatp, Matthieu Gallea
 */
public class Yoshi extends EtreVivant implements Enrolable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4167111499744487374L;

	public Yoshi() {
		super("Yoshi", 40, 20);
	}

	/**
	 * @deprecated
	 * @see EtreVivant
	 */
	@Deprecated
	@Override
	public String mourir() {
		return "Aie Aie Aie!";
	}

	@Override
	public TypeEnrolable getTypeEnrolable() {
		return TypeEnrolable.YOSHI;
	}

	/**
	 * Obtient les mouvents particuliers du cavalier
	 */
	@Override
	public LinkedHashSet<Move> listeDeplacementsPossible(Plateau p, EquipeType j1, char lettre, int position) {
		LinkedHashSet<Move> result = new LinkedHashSet<>();
		Position pos = new Position(lettre, position);
		for (int i = -2; i <= 2; i += 4) {
			for (int j = -1; j <= 1; j += 2) {
				try {
					Position move = new Position((char) (lettre + i), position + j);
					if (!p.estOccupe(move) || p.estOccupe(move) && p.isEnemy(move, j1))
						result.add(new Move(pos, move));
				} catch (IllegalArgumentException iae) {
					// the move isn't possible
				}
				try {
					Position move1 = new Position((char) (lettre + j), position + i);
					if (!p.estOccupe(move1) || p.estOccupe(move1) && p.isEnemy(move1, j1))
						result.add(new Move(pos, move1));
				} catch (IllegalArgumentException iae) {
					// the move isn't possible
				}
			}
		}
		return result;
	}

	@Override
	public String getImage() {
		return "yoshi.png";
	}
}
