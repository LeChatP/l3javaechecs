package edu.mgeb.protagonistes;

import java.util.LinkedHashSet;

import edu.mgeb.controleur.ControleurDeplacement;
import edu.mgeb.plateau.Move;
import edu.mgeb.plateau.Plateau;
import edu.mgeb.plateau.Position;

/**
 * Classe définissant le Fou aux echecs
 * 
 * @author lechatp, Matthieu Gallea
 */
public class Koopa extends EtreVivant implements Enrolable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2695762791261743499L;

	public Koopa() {
		super("Koopa", 60, 10);
	}

	/**
	 * @deprecated
	 * @see EtreVivant
	 */
	@Deprecated
	@Override
	public String mourir() {
		return "We will see next time my brother";
	}

	@Override
	public TypeEnrolable getTypeEnrolable() {
		return TypeEnrolable.KOOPA;
	}

	/**
	 * Peut se déplacer dans toutes les diagonales en rencontrant un ennemi.
	 */
	@Override
	public LinkedHashSet<Move> listeDeplacementsPossible(Plateau p, EquipeType j1, char lettre, int position) {
		Position pos = new Position(lettre, position);
		ControleurDeplacement cp = new ControleurDeplacement();
		cp.line(p, pos, j1, 1, 1);
		cp.line(p, pos, j1, -1, 1);
		cp.line(p, pos, j1, 1, -1);
		cp.line(p, pos, j1, -1, -1);
		return cp.getDeplacements();
	}

	@Override
	public String getImage() {
		return "koopa.png";
	}

}
