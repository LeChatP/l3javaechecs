package edu.mgeb.controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import edu.mgeb.plateau.Position;
import edu.mgeb.vue.ILivre;
import edu.mgeb.vue.JAction;
import edu.mgeb.vue.JCase;
import edu.mgeb.vue.JFenetre;
import edu.mgeb.vue.JPrompt;
import edu.mgeb.vue.Promptable;
import jiconfont.icons.font_awesome.FontAwesome;
import jiconfont.swing.IconFontSwing;

/**
 * Contrôleur de l'interface graphique, il fait interface entre les actions de
 * l'interface et les actions du jeu. Il permet donc de filtrer les informations
 * utiles pour le jeu.
 * 
 * @author lechatp, Matthieu Gallea, Aubin BERINGUET
 */
public class ControleurGUI implements MouseListener, ActionListener {

	/**
	 * La fenêtre
	 */
	public JFenetre jfenetre;
	/**
	 * L'action à executer si le clic souris est correct
	 */
	public PlayerAction action;
	/**
	 * L'action à exécuter sauvegardée
	 */
	public PlayerAction saved;
	/**
	 * L'action a exécuter si un bouton ne concerne pas l'interface graphique
	 */
	public ButtonAction savingAction;

	/**
	 * L'action a exécuter si un bouton ne concerne pas l'interface graphique
	 */
	public ButtonAction onlineAction;
	private ControleurMessage messageControleur;

	/**
	 * Constructeur de l'interface graphique
	 * 
	 * @param savingAction l'action pour la sauvegarde
	 */
	public ControleurGUI() {
		IconFontSwing.register(FontAwesome.getIconFont());
		this.jfenetre = new JFenetre(this, this);
		this.jfenetre.setCaseListener(this);
	}

	public void setSavingAction(ButtonAction savingAction) {
		this.savingAction = savingAction;
	}

	public void setOnlineAction(ButtonAction onlineAction) {
		this.onlineAction = onlineAction;
	}

	/**
	 * Renvoie la version générique de la fenêtre
	 * 
	 * @return L'interface de l'UI
	 */
	public ILivre getLivre() {
		return this.jfenetre;
	}

	/**
	 * Attribue une action lors du clic sur une case
	 * 
	 * @param action
	 */
	public void setActionListener(PlayerAction action) {
		this.action = action;
	}

	/**
	 * Lors du clic, on vérifie que c'est une case.
	 */
	@Override
	public void mouseClicked(MouseEvent mouse) {
		if (this.action != null && mouse.getComponent() instanceof JCase) {
			JCase jcase = (JCase) mouse.getComponent();
			this.action.action(new Position((char) (jcase.getLocation().x), jcase.getLocation().y));
		}
	}

	/**
	 * on implémente pas lorsqu'on survole
	 */
	@Override
	public void mouseEntered(MouseEvent arg0) {
		return;
	}

	/**
	 * on implémente pas lorsqu'on survole
	 */
	@Override
	public void mouseExited(MouseEvent arg0) {
		return;
	}

	/**
	 * on implémente pas lorsqu'on presse le bouton
	 */
	@Override
	public void mousePressed(MouseEvent arg0) {
		return;
	}

	/**
	 * on implémente pas lorsqu'on relache le bouton
	 */
	@Override
	public void mouseReleased(MouseEvent arg0) {
		return;
	}

	/**
	 * Rends visible l'interface graphique
	 */
	public void setVisible(boolean b) {
		this.jfenetre.setVisible(b);
	}

	/**
	 * Supprime le GUI
	 */
	public void dispose() {
		this.jfenetre.dispose();
	}

	/**
	 * Ferme le GUI et ferme le programme
	 */
	public void close() {
		this.jfenetre.close();
	}

	/**
	 * Interface l'action de la barre pour y appeler l'interface avec l'action en
	 * paramètre Permet de ne plus à avoir transformer sa valeur.
	 */
	@Override
	public void actionPerformed(ActionEvent event) {
		JAction action = JAction.valueOf(event.getActionCommand());
		switch (action) {
		default:
		case LOAD:
		case SAVEAS:
		case SAVE:
		case NEW:
			this.savingAction.actionPerformed(action);
			break;
		case NEWONLINE:
		case JOINONLINE:
		case PAUSE:
		case REPRENDRE:
			this.onlineAction.actionPerformed(action);
			break;
		case EXIT:
			this.jfenetre.close();
		case APROPOS:
			new JPrompt().message(
					"Ce jeu d'Echecs vous est présenté par : Eddie BILLOIR, Matthieu GALLEA et Aubin BERINGUET");
			break;
		case SEND:
			if (this.messageControleur.actionPerformed(action))
				this.jfenetre.resetMessageInput();
			break;
		}

	}

	public Promptable getPrompt() {
		return this.jfenetre.getPrompt();
	}

	public void setMessageAction(ControleurMessage controleurMessage) {
		this.messageControleur = controleurMessage;

	}

	public String getMessage() {
		return this.jfenetre.getMessage();
	}

	public void viderDescription() {
		this.jfenetre.viderDesc();
	}

	public void setEnabledInput(boolean b) {
		this.jfenetre.setEnabledInput(b);
	}

	public void setPause() {
		this.jfenetre.setCaseListener(null);
		this.getLivre().afficherMessage("Jeu en pause");
	}
}
