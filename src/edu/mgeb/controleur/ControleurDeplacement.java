package edu.mgeb.controleur;

import java.util.LinkedHashSet;

import edu.mgeb.plateau.Move;
import edu.mgeb.plateau.Plateau;
import edu.mgeb.plateau.Position;
import edu.mgeb.protagonistes.EquipeType;

/**
 * Contrôleur permettant de générer les mouvements possible d'un pion.
 * 
 * @author lechatp, Matthieu Gallea
 */
public class ControleurDeplacement {

	/**
	 * Liste des déplacements générés par l'objet. Celles-ci sont unique donc en Set
	 */
	private LinkedHashSet<Move> deplacements;

	/**
	 * Constructeur initialisant les variables.
	 */
	public ControleurDeplacement() {
		this.deplacements = new LinkedHashSet<>();
	}

	/**
	 * Retourne tous les déplacements générés par l'exécution des fonctions
	 * 
	 * @return La liste des déplacements
	 */
	public LinkedHashSet<Move> getDeplacements() {
		return this.deplacements;
	}

	/**
	 * génère une liste de mouvement en ligne/diagonale en fonction de l'orientation
	 * de la ligne. Cette ligne s'arrête quand il y a un allié et si il croise un
	 * ennemi il ajoute le mouvement sur l'ennemi
	 * 
	 * @param p            le plateau
	 * @param pos          la position initiale
	 * @param j1           le jouaur qui souhaite obtenir la liste des mouvements
	 * @param orientationX l'orientation en X sur le plateau pour faire la
	 *                     ligne/diagonale
	 * @param orientationY l'orientation en Y sur le plateau pour faire la
	 *                     ligne/diagonale
	 */
	public void line(Plateau p, Position pos, EquipeType j1, int orientationX, int orientationY) {
		Position checkPos = null;
		try {
			checkPos = new Position((char) (pos.getLettre() + orientationX), pos.getPosition() + orientationY);

			while (!p.estOccupe(checkPos)) {
				this.deplacements.add(new Move(pos, checkPos));
				if (orientationX > 0)
					orientationX++;
				else if (orientationX < 0)
					orientationX--;
				if (orientationY > 0)
					orientationY++;
				else if (orientationY < 0)
					orientationY--;
				checkPos = new Position((char) (pos.getLettre() + orientationX), pos.getPosition() + orientationY);
			}
			if (p.isEnemy(checkPos, j1)) // si ennemi alors déplacement possible
				this.deplacements.add(new Move(pos, checkPos));
		} catch (IllegalArgumentException e) {
			return; // quand on sort des limites, on arrête l'ajout de Mouvements
		}

	}
}
