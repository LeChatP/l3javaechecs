package edu.mgeb.controleur;

import edu.mgeb.vue.JAction;

/**
 * Interface pour translater un clic de boutton en y passant son type en
 * paramètre
 * 
 * @author lechatp, Matthieu Gallea
 */
public interface ButtonAction {
	/**
	 * Fonction exécutée quand un bouton du menu est cliqué
	 * 
	 * @param action
	 * @return si l'action a été exécutée avec succès ou non
	 */
	public boolean actionPerformed(JAction action);
}
