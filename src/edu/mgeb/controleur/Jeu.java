package edu.mgeb.controleur;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.rmi.RemoteException;

import edu.mgeb.affrontement.Resume;
import edu.mgeb.client.CommunicationClientImpl;
import edu.mgeb.commun.PartieEnLigne;
import edu.mgeb.commun.UnallowedMoveException;
import edu.mgeb.plateau.Case;
import edu.mgeb.plateau.Move;
import edu.mgeb.plateau.Plateau;
import edu.mgeb.plateau.Position;
import edu.mgeb.plateau.Tour;
import edu.mgeb.protagonistes.EquipeType;
import edu.mgeb.vue.ILivre;
import edu.mgeb.vue.JCase;

/**
 * Classe Jeu classe principale du programme
 * 
 * @author lechatp, Matthieu Gallea, Aubin BERINGUET
 */
public class Jeu implements PlayerAction, Serializable {

	/**
	 * l'identifiant pour la sérialisation
	 */
	private static final long serialVersionUID = 674932324820157594L;
	/**
	 * Sa propre instance, accessible en privé uniquement, utile pour le chargement
	 * du jeu. Permet de décharger l'ancienne partie en indiquant au Garbage
	 * collector de le vider
	 */
	private static Jeu jeu;

	/**
	 * L'interface du GUI
	 */
	private transient ILivre livre;

	/**
	 * Le plateau du jeu
	 */
	private Plateau plateau;
	/**
	 * Le gestionnaire de l'interface
	 */
	private transient ControleurGUI gui;
	/**
	 * Le contrôleur de sauvegarde/chargement de partie
	 */
	private ControleurSave save;
	/**
	 * Gestion de l'etat de Selection
	 */
	private Selection selection;
	/**
	 * Le tour du jeu
	 */
	private Tour tour;
	/**
	 * Connexion Client -> serveur
	 */
	private PartieEnLigne remote;

	/**
	 * Connexion Serveur -> Client
	 */
	private CommunicationClientImpl server;
	private ControleurMessage controleurMessage;
	private ControleurOnline controleurOnline;

	/**
	 * instanciation des objets nécessaires pour le jeu
	 */
	public Jeu() {
		this.save = new ControleurSave();
		this.controleurOnline = new ControleurOnline();
		this.gui = new ControleurGUI();
		this.livre = this.gui.getLivre();
		this.plateau = new Plateau(this.livre);
		this.remote = null;
		this.tour = new Tour(this.livre);
		this.controleurMessage = new ControleurMessage(livre, gui, tour);
		this.gui.setMessageAction(this.controleurMessage);
		this.selection = new Selection(this.livre, this.plateau, this.tour);
		this.save.setGame(this);
		this.controleurOnline.setGame(this);
		this.save.setPrompt(this.gui.getPrompt());
		this.controleurOnline.setPrompt(this.gui.getPrompt());
		this.gui.setActionListener(this);
		this.gui.setOnlineAction(this.controleurOnline);
		this.gui.setSavingAction(this.save);

	}

	public static void main(String[] args) {
		jeu = new Jeu();
		jeu.setVisible(true);

	}

	/**
	 * démarre une partie en ligne
	 */
	public void newGame(EquipeType joueur, EquipeType tourDe) {
		this.plateau.initialiserPlateau();
		this.gui.viderDescription();
		this.gui.setEnabledInput(true);
		this.gui.setActionListener(this);
		this.tour.setOnline(joueur, tourDe);
		this.controleurMessage.setPartieEnLigne(this.remote);
	}

	/**
	 * démarre une partie en ligne
	 */
	public void newGame() {
		this.plateau.initialiserPlateau();
		this.tour.setLocal();
		this.gui.setEnabledInput(false);
		this.gui.setActionListener(this);
	}

	public void setVisible(boolean visible) {
		this.gui.setVisible(true);
	}

	public ILivre getILivre() {
		return this.livre;
	}

	/**
	 * Termine la partie sans arrêter le programme
	 */
	public void closeGame() {
		this.gui.dispose();
		this.livre = null;
		this.gui = null;
	}

	public CommunicationClientImpl getServer() {
		return server;
	}

	public void setServer(CommunicationClientImpl server) {
		this.server = server;
	}

	/**
	 * Effectue l'action après un clic joueur
	 */
	@Override
	public void action(Position p) {
		if (JCase.inTimer)
			return; // on vérifie que nous ne sommes pas en animation
		Case thecase = this.plateau.getCase(p);
		if (this.selection.selectionValide(thecase)) // Si le joueur peut sélectionner la case
			this.selection.select(thecase);
		else if (this.selection.enSelection()) { // si on est en sélection
			Move move = this.selection.findMove(thecase);
			if (move != null) // si le mouvement est trouvé dans la sélection
				if (this.tour.enLocal())
					this.doMouvement(move); // en local on fais le mouvement direct
				else
					this.remoteMouvement(move); // en ligne on demande à faire le mouvement
			else
				this.selection.unselect(); // si le mouvement n'est pas possible on réinilialise
		}
	}

	private void doMouvement(Move move) {
		livre.retirerAffichageSelection(move.from().getLettre(), move.from().getPosition());
		livre.retirerMouvementsPossible();
		// on effectue le mouvement par appels de fonctions en cascade
		// Ainsi on conserve la synchronisation des animations de l'interface graphique
		move.move(this.plateau, this.livre, () -> this.finMouvement()); // on exécute le mouvement
	}

	public void remoteMouvement(Move move, Resume resume) {
		livre.retirerAffichageSelection(move.from().getLettre(), move.from().getPosition());
		livre.retirerMouvementsPossible();
		move.move(this.plateau, this.livre, () -> this.finMouvement(), resume);
	}

	private void remoteMouvement(Move move) {
		try {
			this.remote.askMove(move);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnallowedMoveException e) {
			livre.message(e.getMessage());
		}
	}

	private void finMouvement() {
		this.tour.setGagnant(this.plateau.checkGameEnds(this.livre)); // on désigne un vainqueur
		if (this.tour.hasWinner()) {
			this.gui.setActionListener(null);
		}
		this.tour.passerLeTour();
		this.selection.unselect();
	}

	public void finDuJeu(EquipeType gagnant) {
		this.plateau.finDuJeu(this.livre, gagnant);
	}

	/**
	 * Action de chargement du jeu, on relance l'interface graphique, car celle-ci
	 * n'est pas sérialisée
	 * 
	 * @param in l'objet à désérialiser
	 * @throws IOException            les erreurs de lecture du flux
	 * @throws ClassNotFoundException les erreurs en rapport à une classe
	 *                                inexistante (impossible normalement)
	 */
	private void readObject(final ObjectInputStream in) throws IOException, ClassNotFoundException {
		jeu.closeGame();
		in.defaultReadObject();
		this.gui = new ControleurGUI();
		this.save.setPrompt(this.gui.getPrompt());
		this.gui.setSavingAction(this.save);
		this.gui.setActionListener(this);
		this.livre = this.gui.getLivre();
		this.plateau.updatePlateau(this.livre);
		this.selection.afficherSelection();
		this.gui.setVisible(true);
		jeu = this;
		System.gc(); // on appelle le garbage collector pour indiquer de supprimer les variables
						// déréférencées
	}

	public void setPartieEnLigne(PartieEnLigne lookup) {
		this.remote = lookup;
		this.controleurMessage.setPartieEnLigne(lookup);
	}

	public void passerLeTour() {
		this.tour.passerLeTour();
	}

	public void recvAdvMessage(EquipeType equipe, String message) {
		this.controleurMessage.sendMessage(equipe, message);
	}

	public void pause(EquipeType equipe) {
		this.gui.setPause();
		this.controleurOnline.printCode();
		this.tour.setPause(equipe);
		this.tour.afficherTourDe();
	}

	public void reprendre() {
		this.gui.setActionListener(this);
		this.tour.resumePause();
		this.tour.afficherTourDe();
	}

	public Tour getTour() {
		return this.tour;
	}

	public void reprendre(Plateau plateau, Tour tour) {
		this.plateau = plateau;
		this.tour = tour;
		this.tour.setLivre(this.livre);
		this.plateau.updatePlateau(this.livre);
		this.gui.setActionListener(this);
		this.tour.afficherTourDe();
	}

}
