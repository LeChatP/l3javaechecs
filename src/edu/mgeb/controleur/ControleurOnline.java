package edu.mgeb.controleur;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.ExportException;

import edu.mgeb.client.CommunicationClientImpl;
import edu.mgeb.commun.Constants;
import edu.mgeb.commun.CreationPartie;
import edu.mgeb.commun.JoinAddress;
import edu.mgeb.commun.PartieEnLigne;
import edu.mgeb.vue.JAction;
import edu.mgeb.vue.Promptable;

/**
 * Gestion des parties en ligne
 * 
 * @author lechatp, Aubin BERINGUET
 *
 */
public class ControleurOnline implements ButtonAction, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3563693850170653094L;

	/**
	 * L'interface graphique pour IO entre l'utilisateur et le système de sauvegarde
	 */
	private transient Promptable prompt;

	/**
	 * Jeu à sérialiser
	 */
	private Jeu game;

	/**
	 * 
	 */
	private PartieEnLigne partie;

	/**
	 * Url de la partie
	 */
	private JoinAddress address;

	public ControleurOnline() {
		this.prompt = null;
		this.game = null;
	}

	@Override
	public boolean actionPerformed(JAction action) {
		switch (action) {
		case NEWONLINE:
			return this.newOnline();
		case JOINONLINE:
			return this.joinOnline();
		case PAUSE:
			return this.pause();
		case REPRENDRE:
			return this.reprendre();
		default:
			return false;
		}
	}

	private boolean reprendre() {
		if (this.game.getTour().enPause() && this.game.getTour().equipeEnPause() == this.game.getTour().monEquipe()) {
			try {
				this.partie.askResume(this.game.getTour().monEquipe());
				return true;
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		} else if (this.game.getTour().hasWinner() || (this.game.getTour().enPause()
				&& this.game.getTour().equipeEnPause() != this.game.getTour().monEquipe())) {
			return false;
		} else {
			JoinAddress address = this.prompt.askJoinAddress();
			try {
				CreationPartie lobby = (CreationPartie) Naming
						.lookup(Constants.getURL(address.getAddress(), Constants.SERVER_PORT, Constants.CREATE_PATH));
				String reverseurl = this.reverseBind(address.getCode());
				if (lobby.reprendrePartie(reverseurl, address.getCode())) {
					this.partie = (PartieEnLigne) Naming
							.lookup(Constants.getURL(address.getAddress(), Constants.SERVER_PORT, address.getCode()));
					this.game.setPartieEnLigne(this.partie);
					this.game.getILivre().afficherMessage("En attente de l'adversaire");
				} else {
					this.prompt.error("Cette partie n'existe pas sur le serveur");
				}
				return true;
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NotBoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;

	}

	private boolean pause() {
		try {
			this.partie.askPause(this.game.getTour().monEquipe());
			return true;
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public boolean newOnline() {
		InetAddress address = this.prompt.askAddress();
		if (address == null) {
			return false;
		} else {
			try {
				CreationPartie lobby = (CreationPartie) Naming
						.lookup(Constants.getURL(address, Constants.SERVER_PORT, Constants.CREATE_PATH));
				String code = lobby.generateCode();
				this.address = new JoinAddress(address, code);
				lobby = null;
				this.partie = (PartieEnLigne) Naming.lookup(Constants.getURL(address, Constants.SERVER_PORT, code));
				this.game.setPartieEnLigne(this.partie);
				String reverseurl = this.reverseBind(code);
				this.game.getILivre().afficherMessage("Code partie : " + code.toUpperCase());
				return partie.connexionClient(reverseurl);
			} catch (MalformedURLException | RemoteException | NotBoundException | UnknownHostException e) {
				this.prompt.message("Impossible de se connecter au serveur : " + e.getMessage());
				e.printStackTrace();
			}
		}
		return false;

	}

	public boolean joinOnline() {
		this.address = this.prompt.askJoinAddress();
		if (address == null) {
			return false;
		} else {
			try {
				this.partie = (PartieEnLigne) Naming
						.lookup(Constants.getURL(address.getAddress(), Constants.SERVER_PORT, address.getCode()));
				this.game.setPartieEnLigne(partie);
				String reverseurl = this.reverseBind(address.getCode());
				return partie.connexionClient(reverseurl);
			} catch (MalformedURLException | RemoteException | NotBoundException | UnknownHostException e) {
				this.prompt.message("Impossible de se connecter au serveur : " + e.getMessage());
				e.printStackTrace();
			}
			return false;
		}
	}

	private String reverseBind(String code) throws RemoteException, MalformedURLException, UnknownHostException {
		/*
		 * if (System.getSecurityManager() == null) { System.setSecurityManager(new
		 * SecurityManager()); }
		 */
		int port = Constants.CLIENT_PORT;
		boolean connected = false;
		while (!connected) {
			try {
				String url = Constants.getURL(InetAddress.getByName("0.0.0.0"), port, code + String.valueOf(port));
				this.game.setServer(new CommunicationClientImpl(this.game));
				LocateRegistry.createRegistry(port);
				Naming.rebind(url, this.game.getServer());
				connected = true;
				System.out.println("Ecoute sur le port " + port);
				return url;
			} catch (ExportException e) {
				port--;
			}
		}
		return null;

	}

	public void setPrompt(Promptable prompt) {
		this.prompt = prompt;
	}

	public void setGame(Jeu game) {
		this.game = game;
	}

	public void printCode() {
		this.prompt.message("Vous pouvez quitter, et reprendre la partie plus tard, notez ces informations :\nUrl: "
				+ this.address.getAddress().getHostName() + "\nCode : " + this.address.getCode());
	}

}
