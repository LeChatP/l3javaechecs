package edu.mgeb.controleur;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import edu.mgeb.vue.JAction;
import edu.mgeb.vue.Promptable;

/**
 * Gestion de la sauvegarde
 * 
 * @author lechatp, Matthieu Gallea, Aubin BERINGUET
 */
public class ControleurSave implements ButtonAction, Serializable {

	/**
	 * UID pour la sérialisation
	 */
	private static final long serialVersionUID = -8499475012720639001L;

	/**
	 * Nom du fichier
	 */
	private String filename;

	/**
	 * Format de la date du fichier
	 */
	private static final String DATE_FORMAT_NOW = "yyyy-MM-dd_HH.mm.ss";

	/**
	 * Jeu à sérialiser
	 */
	private Jeu game;

	/**
	 * Booléen pour ne plus demander le chemin après l'avoir spécifié une première
	 * fois
	 */
	private boolean init;
	/**
	 * L'interface graphique pour IO entre l'utilisateur et le système de sauvegarde
	 */
	private transient Promptable prompt;

	/**
	 * Controleur pour la sauvegarde
	 */
	public ControleurSave() {
		this.filename = System.getProperty("user.dir") + File.separator + "MarioEchecs" + this.now() + ".sav";
		this.init = true;
		this.prompt = null;
	}

	public void setGame(Jeu game) {
		this.game = game;
	}

	/**
	 * Récupère la date en chaine de caractères
	 * 
	 * @return la date en chaine de caractères
	 */
	private String now() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		return sdf.format(cal.getTime());
	}

	/**
	 * Réattribue le prompt (une fois le chargement effectué)
	 * 
	 * @param promptable
	 */
	public void setPrompt(Promptable promptable) {
		this.prompt = promptable;
	}

	/**
	 * Effectue le traitement pour sauvegarder/charger
	 */
	@Override
	public boolean actionPerformed(JAction action) {
		switch (action) {
		case NEW:
			this.game.newGame();
			return true;
		case SAVE:
			return this.save();
		case SAVEAS:
			return this.saveAs();
		case LOAD:
			return this.load();
		default:
			throw new IllegalArgumentException("This isn't my role to do this");
		}
	}

	/**
	 * Effectue la sauvegarde
	 * 
	 * @return si elle a été effectué correctement ou si il y a eu une erreur
	 */
	public boolean save() {
		if (init) {
			return this.saveAs();
		}
		return this.save(this.filename, this);
	}

	public boolean save(String filename, Object save) {
		ObjectOutputStream oos = null;
		String message = "Sauvegardé";
		try {
			final FileOutputStream fichier = new FileOutputStream(filename);
			oos = new ObjectOutputStream(fichier);
			oos.writeObject(save);
			oos.flush();
		} catch (final IOException e) {
			e.printStackTrace();
			message = e.getLocalizedMessage();
		} finally {
			try {
				if (oos != null) {
					oos.flush();
					oos.close();
				} else {
					if (this.prompt != null)
						this.prompt.error(message);
					return false;
				}
			} catch (final IOException ex) {
				ex.printStackTrace();
				if (this.prompt != null)
					this.prompt.error("Echec à 2 reprises : \n" + ex.getLocalizedMessage());
				return false;
			}
		}
		if (this.prompt != null)
			this.prompt.message(message);
		return true;
	}

	/**
	 * Charge une partie
	 * 
	 * @return si elle a été effectué correctement ou si il y a eu une erreur
	 */
	public boolean load() {
		this.filename = this.prompt.chooseFile(new File(this.filename));
		if (filename == null) {
			return false;
		}
		Object load = this.load(this.filename);
		return load != null && load instanceof Boolean && (Boolean) load;
	}

	public Object load(String path) {
		ObjectInputStream ois = null;
		String message = "Chargé";
		try {
			final FileInputStream fichier = new FileInputStream(path);
			ois = new ObjectInputStream(fichier);
			return ois.readObject();

		} catch (final IOException e) {
			e.printStackTrace();
			message = e.getLocalizedMessage();
		} catch (final ClassNotFoundException e) {
			e.printStackTrace();
			message = e.getLocalizedMessage();
		} finally {
			try {
				if (ois != null) {
					ois.close();
				} else {
					if (this.prompt != null)
						this.prompt.error(message);
					return false;
				}
			} catch (final IOException ex) {
				ex.printStackTrace();
				message = "Echec à 2 reprises : \n" + ex.getLocalizedMessage();
				if (this.prompt != null)
					this.prompt.error(message);
				return false;
			}
		}
		if (this.prompt != null)
			this.prompt.message(message);
		return true;
	}

	/**
	 * Sauvegarde sous un nouveau fichier
	 * 
	 * @return si elle a été effectué correctement ou si il y a eu une erreur
	 * @see save
	 */
	public boolean saveAs() {

		String file = this.prompt.saveFile(new File(this.filename));
		if (file == null) {
			return true;
		}
		this.filename = file;
		this.init = false;
		return this.save();
	}

}
