package edu.mgeb.controleur;

import edu.mgeb.plateau.Position;

/**
 * Interface de l'action du joueur
 * 
 * @author lechatp, Matthieu Gallea
 */
public interface PlayerAction {
	/**
	 * L'action du joueur
	 * 
	 * @param p la position cliquée
	 */
	public void action(Position p);
}
