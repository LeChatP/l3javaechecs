package edu.mgeb.controleur;

import java.rmi.RemoteException;

import edu.mgeb.commun.IllegalMessageException;
import edu.mgeb.commun.PartieEnLigne;
import edu.mgeb.plateau.Tour;
import edu.mgeb.protagonistes.EquipeType;
import edu.mgeb.vue.ILivre;
import edu.mgeb.vue.JAction;

/**
 * 
 * @author lechatp, Aubin BERINGUET
 *
 */
public class ControleurMessage implements ButtonAction {
	private ILivre livre;
	private PartieEnLigne online;
	private ControleurGUI message;
	private Tour tour;

	public ControleurMessage(ILivre livre, ControleurGUI message, Tour tour) {
		this.livre = livre;
		this.message = message;
		this.tour = tour;
	}

	@Override
	public boolean actionPerformed(JAction action) {
		if (action != JAction.SEND)
			return false;
		if (this.online != null)
			try {
				this.online.sendMessage(this.tour.monEquipe(), message.getMessage());
			} catch (RemoteException e) {
				e.printStackTrace();
				return false;
			} catch (IllegalMessageException e) {
				this.livre.message(e.getMessage());
				return false;
			}
		return true;
	}

	public void setPartieEnLigne(PartieEnLigne online) {
		this.online = online;
	}

	public void sendMessage(EquipeType equipe, String message) {
		livre.ecrireDescription(message, true, equipe.toString());
	}

}
