package edu.mgeb.controleur;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.stream.Stream;

import edu.mgeb.plateau.Case;
import edu.mgeb.plateau.Move;
import edu.mgeb.plateau.Plateau;
import edu.mgeb.plateau.Position;
import edu.mgeb.plateau.Tour;
import edu.mgeb.protagonistes.Equipe;
import edu.mgeb.vue.ILivre;

/**
 * Gestion de l'état de Sélection
 * 
 * @author lechatp
 *
 */
public class Selection implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2777160499641948380L;
	/**
	 * La position du pion en sélection
	 */
	private Position from;
	/**
	 * Les mouvements possible du pion en sélection
	 */
	private LinkedHashSet<Move> moves;
	/**
	 * Plateau du jeu
	 */
	private Plateau plateau;
	/**
	 * Le livre du jeu
	 */
	private ILivre livre;
	/**
	 * Le tour de la partie
	 */
	private Tour tour;

	public Selection(ILivre livre, Plateau p, Tour tour) {
		this.plateau = p;
		this.livre = livre;
		this.tour = tour;
		this.from = null;
		this.moves = null;
	}

	public void afficherSelection() {
		if (!enSelection()) {
			this.retireraffichage(this.from);
		} else {
			this.livre.afficherSelection(this.from.getLettre(), this.from.getPosition());
			this.livre.afficherMouvementsPossible(this.moves);
		}
	}

	public void retireraffichage(Position pos) {
		livre.retirerAffichageSelection(this.from.getLettre(), this.from.getPosition());
		livre.retirerMouvementsPossible();
	}

	public void select(Case thecase) {
		this.from = thecase.getPosition();
		this.moves = thecase.getPion().listeDeplacementsPossible(this.plateau, Equipe.findEquipe(thecase.getPion()),
				this.from.getLettre(), this.from.getPosition());
		this.afficherSelection();
	}

	public Move findMove(Case thecase) {
		Stream<Move> stream = this.getMoves().stream().filter(move -> move.to().equals(thecase.getPosition()));
		Optional<Move> optionnal = stream.findAny();
		if (optionnal.isPresent()) { // si le mouvement est dans le Tableau
			return optionnal.get();
		} else
			return null;
	}

	private boolean pionDuJoueur(Case thecase) {
		return this.plateau.getEquipe(this.tour.getTourDe()).getList().contains(thecase.getPion());
	}

	public boolean selectionValide(Case thecase) {
		return !this.enSelection() && // Si on n'est pas en sélection
				thecase != null && this.tour.sonTour() && // Si c'est au tour du joueur actuel
				thecase.estOccupe() && // et que la case est occupée
				this.pionDuJoueur(thecase); // et que le pion appartient à l'équipe qui joue
	}

	public void unselect() {
		if (enSelection())
			this.retireraffichage(from);
		this.from = null;
		this.moves = null;
	}

	public boolean enSelection() {
		return this.from != null;
	}

	public LinkedHashSet<Move> getMoves() {
		return this.moves;
	}
}
