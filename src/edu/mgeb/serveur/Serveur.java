package edu.mgeb.serveur;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

import edu.mgeb.commun.Constants;
import edu.mgeb.commun.SavingService;
import edu.mgeb.serveur.config.ServerInfoConfig;

/**
 * 
 * @author lechatp
 *
 */
public class Serveur {
	private static Serveur serveur;
	private SavingService saving;
	private transient ServerInfoConfig config;
	private transient ServerInfoConfig savingconfig;

	private Serveur() {
		this.config = new ServerInfoConfig(Constants.SRVCONFIGFILE, Constants.DEFAULT_HOST, Constants.SERVER_PORT);
		this.savingconfig = new ServerInfoConfig(Constants.SAVINGCONFIGFILE, Constants.LOCALHOST,
				Constants.SAVING_SERVER_PORT);
		this.connectSavingService();

	}

	private void connectSavingService() {
		try {
			this.saving = (SavingService) Naming
					.lookup(Constants.getURL(InetAddress.getByName(this.savingconfig.getHost()),
							this.savingconfig.getPort(), Constants.SAVING_URL));
		} catch (MalformedURLException | RemoteException | UnknownHostException | NotBoundException e) {
			System.out.println("Impossible de se connecter au service de sauvegarde");
		}
	}

	public static void main(String[] args) {
		Serveur srv = getServeur();
		try {
			/**
			 * if (System.getSecurityManager() == null) { System.setSecurityManager(new
			 * SecurityManager()); }
			 */

			LocateRegistry.createRegistry(srv.getConfig().getPort());
			CreationPartieImpl creation = new CreationPartieImpl();
			String url = Constants.getURL(InetAddress.getByName(srv.getConfig().getHost()), srv.getConfig().getPort(),
					Constants.CREATE_PATH);
			Naming.rebind(url, creation);
			System.out.println("Serveur démarré, prêt à te démarrer.");
		} catch (RemoteException | UnknownHostException | MalformedURLException e) {
			System.out.println(
					"Impossible de démarrer le serveur. Donnez-moi une interface, un port et je tourne sans problèmes.");
			System.exit(-1);
		}

	}

	public static Serveur getServeur() {
		return serveur == null ? serveur = new Serveur() : serveur;
	}

	public SavingService getSavingServeur() {
		if (this.saving == null) {
			this.connectSavingService();
		}
		return this.saving;
	}

	public ServerInfoConfig getConfig() {
		return config;
	}

}
