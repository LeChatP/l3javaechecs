package edu.mgeb.serveur;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import edu.mgeb.commun.Constants;
import edu.mgeb.serveur.config.ServerInfoConfig;

public class SavingServeur extends Thread {
	private static SavingServeur serveur;
	private transient ServerInfoConfig config;
	private SavingServiceImpl saving;
	private Registry registry;

	private SavingServeur() {
		this.setDaemon(true);
		this.config = new ServerInfoConfig(Constants.SAVINGCONFIGFILE, Constants.DEFAULT_HOST,
				Constants.SAVING_SERVER_PORT);
		this.saving = new SavingServiceImpl();
		try {
			this.registry = LocateRegistry.createRegistry(this.getServerConfig().getPort());
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		try {
			String url = Constants.getURL(InetAddress.getByName(this.getServerConfig().getHost()),
					this.getServerConfig().getPort(), Constants.SAVING_URL);
			Naming.rebind(url, saving);
			System.out.println("Serveur de sauvegarde démarré, prêt à tout mémoriser.");
		} catch (RemoteException | UnknownHostException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		super.run();
		while (true) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				System.out.println("Exiting");
				System.exit(0);
			}
		}
	}

	public static void main(String[] args) {
		getServeur().start();
		try {
			getServeur().join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static SavingServeur getServeur() {
		return serveur == null ? serveur = new SavingServeur() : serveur;
	}

	private ServerInfoConfig getServerConfig() {
		return config;
	}

	public SavingServiceImpl getSavingService() {
		return saving;
	}
}
