package edu.mgeb.serveur;

import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.RemoteServer;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;

import edu.mgeb.affrontement.Resume;
import edu.mgeb.commun.CommunicationClient;
import edu.mgeb.commun.IllegalMessageException;
import edu.mgeb.commun.PartieEnLigne;
import edu.mgeb.commun.UnallowedMoveException;
import edu.mgeb.plateau.Case;
import edu.mgeb.plateau.Move;
import edu.mgeb.plateau.Plateau;
import edu.mgeb.plateau.Tour;
import edu.mgeb.protagonistes.EquipeType;
import edu.mgeb.vue.LivreServeur;

/**
 * 
 * @author lechatp, Aubin BERINGUET
 *
 */
public class PartieEnLigneImpl extends UnicastRemoteObject implements PartieEnLigne {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8349317272812002628L;
	private String[] clients;
	private CommunicationClient[] communication;
	private int compteurClients;
	private Plateau plateau;
	private Tour tour;
	private transient LivreServeur livre;
	private String code;
	private transient boolean resume;

	protected PartieEnLigneImpl() throws RemoteException {
		super();
		this.initVars();
	}

	protected PartieEnLigneImpl(Plateau plateau, Tour tour) throws RemoteException {
		super();
		this.initVars(plateau, tour);
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void initVars() {
		this.communication = new CommunicationClient[2];
		this.clients = new String[2];
		this.compteurClients = 0;
		this.livre = new LivreServeur();
		this.plateau = new Plateau(this.livre);
		this.tour = new Tour(this.livre);
		this.resume = false;
	}

	public void initVars(Plateau plateau, Tour tour) {
		this.communication = new CommunicationClient[2];
		this.clients = new String[2];
		this.compteurClients = 0;
		this.livre = new LivreServeur();
		this.plateau = plateau;
		this.tour = tour;
		this.tour.setLivre(this.livre);
		this.resume = true;
	}

	@Override
	public boolean connexionClient(String urlClient) throws RemoteException {
		/*
		 * if (System.getSecurityManager() == null) { System.setSecurityManager(new
		 * SecurityManager()); }
		 */
		if (this.compteurClients == 2) {
			return false;
		}

		try {
			this.clients[this.compteurClients] = RemoteServer.getClientHost();
			this.communication[this.compteurClients] = (CommunicationClient) Naming.lookup(urlClient);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		this.compteurClients++;
		if (this.compteurClients == 2) {
			this.tour.setLocal();
			if (this.resume) {
				this.sendResume(this.plateau, this.tour);
			} else if (this.communication[0].commencerPartie(EquipeType.MARIO, this.tour.getTourDe())
					&& this.communication[1].commencerPartie(EquipeType.LAPINS, this.tour.getTourDe())) {
				this.plateau.initialiserPlateau();
				return true;
			} else
				return false;
		}
		return true;
	}

	private boolean moveAllowed(Move move) {
		Case thecase = this.plateau.getCase(move.from());
		Case to = this.plateau.getCase(move.to());
		try {
			return !this.tour.hasWinner() && // si la partie n'est pas terminée
					thecase.estOccupe() && // si la case est occupée
					this.plateau.getEquipe(this.tour.getTourDe()).getList().contains(thecase.getPion()) && // si le pion
																											// de la
																											// case
																											// appartient
																											// au joueur
					this.clients[this.tour.getTourDe().estLapins() ? 1 : 0]
							.equalsIgnoreCase(RemoteServer.getClientHost())
					&& // si le client qui appelle la fonction est le bon joueur
					(!to.estOccupe() || this.isCombat(move)); // si la case n'est pas occupée alors il retourne OK sinon
																// il vérifie que la case destination est un pion ennemi
		} catch (ServerNotActiveException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	private boolean isCombat(Move move) {
		Case to = this.plateau.getCase(move.to());
		return this.plateau.getEquipe(this.tour.getTourDe().adversaire()).getList().contains(to.getPion());
	}

	@Override
	public void askMove(Move move) throws RemoteException, UnallowedMoveException {
		if (this.moveAllowed(move)) {
			Resume resume = move.move(this.plateau, this.livre, null);
			this.sendMove(move, resume);
			this.tour.setGagnant(this.plateau.checkGameEnds(this.livre)); // on désigne un vainqueur
			if (this.tour.hasWinner()) {
				this.initVars(); // si la partie s'est terminé, on efface
			} else {
				this.tour.passerLeTour();
			}
			return;
		}
		throw new UnallowedMoveException("Tu ne peux pas faire ce mouvement");

	}

	private void sendMove(Move move, Resume resume) {
		try {
			this.communication[0].sendMove(move, resume);
			this.communication[1].sendMove(move, resume);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void sendMessage(EquipeType equipe, String message) throws IllegalMessageException {
		try {
			if (!this.clients[equipe.estLapins() ? 1 : 0].equalsIgnoreCase(RemoteServer.getClientHost()))
				throw new IllegalMessageException("You aren't the player that you called");
			try {
				this.communication[0].sendMessage(equipe, message);
				this.communication[1].sendMessage(equipe, message);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (ServerNotActiveException e2) {
			e2.printStackTrace();
		}

	}

	@Override
	public void askPause(EquipeType equipe) throws RemoteException {
		try {
			Serveur.getServeur().getSavingServeur().save(new OnlineSave(this.code, this.plateau, this.tour));
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.tour.setPause(equipe);
		new Thread(() -> { // on exécute dans un thread pour libérer l'interface graphique avant d'envoyer
							// un évenement réseau
			this.sendPause(equipe);
		}).start();
	}

	private void sendPause(EquipeType equipe) {
		try {
			this.communication[0].partieEnPause(equipe);
			this.communication[1].partieEnPause(equipe);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void askResume(EquipeType equipe) {
		if (this.tour.enPause() && this.tour.equipeEnPause() == equipe) {
			this.tour.resumePause();
			this.sendResume();
		}
	}

	private void sendResume() {
		try {
			// une boucle for est moins performant que 2 instructions explicite
			this.communication[0].resume();
			this.communication[1].resume();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	private void sendResume(Plateau plateau, Tour tour) {
		try {
			// une boucle for est moins performant que 2 instructions explicite
			this.communication[0].resume(plateau, tour);
			this.communication[1].resume(plateau, tour);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

}
