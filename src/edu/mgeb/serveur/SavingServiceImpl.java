package edu.mgeb.serveur;

import java.io.File;
import java.io.Serializable;
import java.rmi.RemoteException;

import edu.mgeb.commun.SavingService;
import edu.mgeb.controleur.ControleurSave;

/**
 * @author lechatp
 *
 */
public class SavingServiceImpl implements SavingService, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7843478721573971641L;
	private transient ControleurSave saving;

	public SavingServiceImpl() {
		this.saving = new ControleurSave();
	}

	@Override
	public boolean save(OnlineSave save) throws RemoteException {
		return this.saving.save(System.getProperty("user.dir") + File.separator + save.getCode() + ".sav",
				new OnlineSave(save.getCode(), save.getPlateau(), save.getTour()));
	}

	@Override
	public OnlineSave load(String code) throws RemoteException {
		Object returned = this.saving.load(System.getProperty("user.dir") + File.separator + code + ".sav");
		if (returned != null && returned instanceof OnlineSave) {
			return (OnlineSave) returned;
		}
		return null;
	}
}
