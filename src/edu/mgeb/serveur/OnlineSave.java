package edu.mgeb.serveur;

import java.io.Serializable;

import edu.mgeb.plateau.Plateau;
import edu.mgeb.plateau.Tour;

/**
 * 
 * @author lechatp
 *
 */
public class OnlineSave implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4582232763830570334L;
	private Plateau plateau;
	private Tour tour;
	private String code;

	public OnlineSave(String code, Plateau plateau, Tour tour) {
		this.code = code;
		this.plateau = plateau;
		this.tour = tour;
	}

	public Plateau getPlateau() {
		return plateau;
	}

	public void setPlateau(Plateau plateau) {
		this.plateau = plateau;
	}

	public Tour getTour() {
		return tour;
	}

	public void setTour(Tour tour) {
		this.tour = tour;
	}

	public String getCode() {
		return code;
	}

}
