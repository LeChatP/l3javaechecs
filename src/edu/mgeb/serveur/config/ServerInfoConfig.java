package edu.mgeb.serveur.config;

import java.util.Properties;

import edu.mgeb.commun.Constants;

public class ServerInfoConfig extends MConfig {

	private static final String PORT_KEY = "port";

	private static final String HOST_KEY = "host";

	protected String host;
	protected String port;

	public ServerInfoConfig(String configPath, String defaultHost, int serverPort) {
		super(configPath);
		this.host = defaultHost;
		this.port = String.valueOf(serverPort);
		this.load();
	}

	@Override
	protected void setProperties(Properties props) {
		props.setProperty(HOST_KEY, this.host);
		props.setProperty(PORT_KEY, this.port);
	}

	public String getHost() {
		return this.getOrDefault(HOST_KEY, this.host);
	}

	public int getPort() {
		return Integer.valueOf(this.getOrDefault(PORT_KEY, this.port));
	}
}
