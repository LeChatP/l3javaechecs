package edu.mgeb.serveur.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import java.util.logging.Logger;

public abstract class MConfig {

	protected String configPath;
	private File configFile;
	private Properties configProps;

	public MConfig(String configPath) {
		this.configPath = configPath;
		this.configFile = new File(this.configPath);
	}

	protected void load() {
		if (!this.configFile.exists()) {
			try {
				saveProperties();
			} catch (IOException e) {
				Logger.getLogger("MConfig").warning("Unable to save default properties.");
			}
		}
		try {
			loadProperties();
		} catch (IOException ex) {
			Logger.getLogger("MConfig").warning("Unable to load properties, getting default");
		}
	}

	private void loadProperties() throws IOException {
		Properties defaultProps = new Properties();
		// sets default properties
		this.setProperties(defaultProps);

		this.configProps = new Properties(defaultProps);

		// loads properties from file
		InputStream inputStream = new FileInputStream(configFile);
		this.configProps.load(inputStream);
		inputStream.close();
	}

	private void saveProperties() throws IOException {
		Properties defaultProps = new Properties();
		this.setProperties(defaultProps);
		OutputStream outputStream = new FileOutputStream(configFile);
		defaultProps.store(outputStream, "host setttings");
		outputStream.close();
	}

	protected String getOrDefault(String key, String defaultValue) {
		return (String) this.configProps.getOrDefault(key, defaultValue);
	}

	protected abstract void setProperties(Properties props);
}
