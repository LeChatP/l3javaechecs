package edu.mgeb.serveur;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.security.SecureRandom;
import java.util.UUID;

import edu.mgeb.commun.Constants;
import edu.mgeb.commun.CreationPartie;
import edu.mgeb.commun.PartieEnLigne;

/**
 * 
 * @author lechatp, Aubin BERINGUET
 *
 */
public class CreationPartieImpl extends UnicastRemoteObject implements CreationPartie {

	/**
	 * 
	 */
	private static final long serialVersionUID = -987424091051419950L;

	protected CreationPartieImpl() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	private String getCode() {
		// TODO Auto-generated method stub
		String chars = "";
		for (int i = 0; i < 4; i++) {
			chars += UUID.randomUUID().toString().replace("-", "");
		}
		SecureRandom random = new SecureRandom();
		int beginIndex = random.nextInt(100); // Begin index + length of your string < data length
		int endIndex = beginIndex + NB_CHARS_CODE; // Length of string which you want

		return chars.substring(beginIndex, endIndex).toUpperCase();
	}

	@Override
	public String generateCode() throws RemoteException {
		String code = getCode();
		try {

			String url = Constants.getURL(InetAddress.getLocalHost(), Constants.SERVER_PORT, code);
			// On instancie l'objet manipulable à distance
			PartieEnLigneImpl partieEnLigneImpl = new PartieEnLigneImpl();
			partieEnLigneImpl.setCode(code);

			System.out.println("Enregistrement de l'objet avec l'url : " + url);
			// On l'enregistre dans l'annuaire pour autoriser les connexions
			// On ne référence pas la partie dans l'annuaire carla partie est privée
			Naming.rebind(url, partieEnLigneImpl);

			System.out.println("Serveur lancé");

		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return code;
	}

	@Override
	public boolean reprendrePartie(String urlClient, String code) throws RemoteException {
		String url;
		try { // on tente de se connecter si la partie existe déjà sur le serveur
			url = Constants.getURL(InetAddress.getLocalHost(), Constants.SERVER_PORT, code);
			PartieEnLigne r = (PartieEnLigne) Naming.lookup(url);
			r.connexionClient(urlClient);
			return true;
		} catch (UnknownHostException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
			return false;
		} catch (MalformedURLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
			return false;
		} catch (RemoteException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
			return false;
		} catch (NotBoundException e2) {
			System.out.println("Tentative de charger le fichier");
		}
		// on tente de charger la partie depuis le stockage serveur
		OnlineSave save = null;
		try {
			save = Serveur.getServeur().getSavingServeur().load(code);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (save != null) {
			PartieEnLigneImpl partieEnLigneImpl = new PartieEnLigneImpl(save.getPlateau(), save.getTour());
			try {
				url = Constants.getURL(InetAddress.getLocalHost(), Constants.SERVER_PORT, code);
				Naming.bind(url, partieEnLigneImpl);
				partieEnLigneImpl.connexionClient(urlClient);
				return true;
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			} catch (AlreadyBoundException e) {
				e.printStackTrace();
				return false;
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
		}
		return false;
	}

}
