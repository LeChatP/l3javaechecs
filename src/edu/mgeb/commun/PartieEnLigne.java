package edu.mgeb.commun;

import java.rmi.Remote;
import java.rmi.RemoteException;

import edu.mgeb.plateau.Move;
import edu.mgeb.protagonistes.EquipeType;

/**
 * Communication Client vers Serveur
 * 
 * @author lechatp, Aubin BERINGUET
 *
 */
public interface PartieEnLigne extends Remote {
	public boolean connexionClient(String urlClient) throws RemoteException;

	public void askMove(Move move) throws RemoteException, UnallowedMoveException;

	public void sendMessage(EquipeType equipe, String message) throws RemoteException, IllegalMessageException;

	void askPause(EquipeType equipe) throws RemoteException;

	void askResume(EquipeType equipe) throws RemoteException;
}
