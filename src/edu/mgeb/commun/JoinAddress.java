package edu.mgeb.commun;

import java.net.InetAddress;

/**
 * 
 * @author lechatp, Aubin BERINGUET
 *
 */
public class JoinAddress {
	private InetAddress address;
	private String code;

	public JoinAddress(InetAddress address, String code) {
		this.address = address;
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public InetAddress getAddress() {
		return address;
	}

}
