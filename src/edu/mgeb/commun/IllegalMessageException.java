package edu.mgeb.commun;

/**
 * 
 * @author lechatp
 *
 */
public class IllegalMessageException extends Exception {

	private static final long serialVersionUID = 9127362774494483281L;

	public IllegalMessageException(String message) {
		super(message);
	}

}
