package edu.mgeb.commun;

/**
 * 
 * @author lechatp
 *
 */
public class UnallowedMoveException extends Exception {

	private static final long serialVersionUID = 9127362774494483281L;

	public UnallowedMoveException(String message) {
		super(message);
	}

}
