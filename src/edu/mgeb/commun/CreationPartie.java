package edu.mgeb.commun;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * 
 * @author Aubin BERINGUET
 *
 */
public interface CreationPartie extends Remote {

	public static final int NB_CHARS_CODE = 5;

	public String generateCode() throws RemoteException;

	boolean reprendrePartie(String urlClient, String code) throws RemoteException;

}
