package edu.mgeb.commun;

import java.net.InetAddress;

/**
 * 
 * @author lechatp, Aubin BERINGUET
 *
 */
public class Constants {
	public static final int CLIENT_PORT = 1098;
	public static final int SERVER_PORT = 1099;
	public static final String CREATE_PATH = "create";
	public static final int SAVING_SERVER_PORT = 1100;
	public static final String SAVING_URL = "save";
	public static final String DEFAULT_HOST = "0.0.0.0";
	public static final String LOCALHOST = "localhost";
	public static final String SRVCONFIGFILE = "server.properties";
	public static final String SAVINGCONFIGFILE = "saving.properties";

	public static String getURL(InetAddress address, int port, String path) {
		return "rmi://" + address.getHostAddress() + ":" + port + "/" + path;
	}
}
