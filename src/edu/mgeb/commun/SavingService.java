package edu.mgeb.commun;

import java.rmi.Remote;
import java.rmi.RemoteException;

import edu.mgeb.serveur.OnlineSave;

/**
 * 
 * @author lechatp
 *
 */
public interface SavingService extends Remote {
	public boolean save(OnlineSave save) throws RemoteException;

	public OnlineSave load(String code) throws RemoteException;
}
