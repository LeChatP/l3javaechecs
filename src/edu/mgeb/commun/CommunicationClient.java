package edu.mgeb.commun;

import java.rmi.Remote;
import java.rmi.RemoteException;

import edu.mgeb.affrontement.Resume;
import edu.mgeb.plateau.Move;
import edu.mgeb.plateau.Plateau;
import edu.mgeb.plateau.Tour;
import edu.mgeb.protagonistes.EquipeType;

/**
 * 
 * @author lechatp, Aubin BERINGUET
 *
 */
public interface CommunicationClient extends Remote {
	public boolean commencerPartie(EquipeType role, EquipeType commence) throws RemoteException;

	public void sendMove(Move move, Resume resume) throws RemoteException;

	public void terminerPartie(EquipeType gagnant) throws RemoteException;

	public void sendMessage(EquipeType equipe, String message) throws RemoteException;

	public void partieEnPause(EquipeType equipe) throws RemoteException;

	public void resume() throws RemoteException;

	public void resume(Plateau plateau, Tour tour) throws RemoteException;
}
